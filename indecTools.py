"""
This module is intended to be used as a tool-box for
facilitating the comprehension of the methodologies
used by INDEC (Instituto Nacional de Estadisticas y Censos)
from Argentina to estimate various sociological and 
demographic variables.

Author: Javier Marti
Date: 2019/04/07
License: GPL (General Public License)
Last updated: 2019/06/09
"""

# importamos modulos necesarios
import datetime
import numpy as np
import pandas as pd
from copy import deepcopy

########################################
### Metadata from other INDEC report ###
# Factores de conversion respecto de un adulto varon para
# el calculo del valor de referencia de las canastas basicas
# alimentaria y total.
refUnitMap = {2: {0.5: 0.28, 0.75: 0.35, 1: 0.37, 2: 0.46, 3: 0.51, 4: 0.55, 5: 0.60, 6: 0.64,
                  7: 0.66, 8: 0.68, 9: 0.69, 10: 0.70, 11: 0.72, 12: 0.74, 13: 0.76, 14: 0.76,
                  15: 0.77, 16: 0.77, 17: 0.77, 18: 0.77, 19: 0.77, 20: 0.77, 21: 0.77, 22: 0.77,
                  23: 0.77, 24: 0.77, 25: 0.77, 26: 0.77, 27: 0.77, 28: 0.77, 29: 0.77, 30: 0.77,
                  31: 0.77, 32: 0.77, 33: 0.77, 34: 0.77, 35: 0.77, 36: 0.77, 37: 0.77, 38: 0.77,
                  39: 0.77, 40: 0.77, 41: 0.77, 42: 0.77, 43: 0.77, 44: 0.77, 45: 0.77, 46: 0.76,
                  47: 0.76, 48: 0.76, 49: 0.76, 50: 0.76, 51: 0.76, 52: 0.76, 53: 0.76, 54: 0.76,
                  55: 0.76, 56: 0.76, 57: 0.76, 58: 0.76, 59: 0.76, 60: 0.76, 61: 0.67, 62: 0.67,
                  63: 0.67, 64: 0.67, 65: 0.67, 66: 0.67, 67: 0.67, 68: 0.67, 69: 0.67, 70: 0.67,
                  71: 0.67, 72: 0.67, 73: 0.67, 74: 0.67, 75: 0.67, 76: 0.63, 77: 0.63, 78: 0.63,
                  79: 0.63, 80: 0.63, 81: 0.63, 82: 0.63, 83: 0.63, 84: 0.63, 85: 0.63, 86: 0.63,
                  87: 0.63, 88: 0.63, 89: 0.63, 90: 0.63, 91: 0.63, 92: 0.63, 93: 0.63, 94: 0.63,
                  95: 0.63, 96: 0.63, 97:  0.63, 98: 0.63, 99: 0.63, 100: 0.63, 101: 0.63,
                  102: 0.63, 103: 0.63, 104: 0.63, 105: 0.63, 106: 0.63, 107: 0.63, 108: 0.63,
                  109: 0.63, 110: 0.63},
              1: {0.5: 0.28, 0.75: 0.35, 1: 0.37, 2: 0.46, 3: 0.51, 4: 0.55, 5: 0.60, 6: 0.64,
                  7: 0.66, 8: 0.68, 9: 0.69, 10: 0.79, 11: 0.82, 12: 0.85, 13: 0.90, 14: 0.96,
                  15: 1.00, 16: 1.03, 17: 1.04, 18: 1.02, 19: 1.02, 20: 1.02, 21: 1.02, 22: 1.02,
                  23: 1.02, 24: 1.02, 25: 1.02, 26: 1.02, 27: 1.02, 28: 1.02, 29: 1.02, 30: 1.00,
                  31: 1.00, 32: 1.00, 33: 1.00, 34: 1.00, 35: 1.00, 36: 1.00, 37: 1.00, 38: 1.00,
                  39: 1.00, 40: 1.00, 41: 1.00, 42: 1.00, 43: 1.00, 44: 1.00, 45: 1.00, 46: 1.00,
                  47: 1.00, 48: 1.00, 49: 1.00, 50: 1.00, 51: 1.00, 52: 1.00, 53: 1.00, 54: 1.00,
                  55: 1.00, 56: 1.00, 57: 1.00, 58: 1.00, 59: 1.00, 60: 1.00, 61: 0.83, 62: 0.83,
                  63: 0.83, 64: 0.83, 65: 0.83, 66: 0.83, 67: 0.83, 68: 0.83, 69: 0.83, 70: 0.83,
                  71: 0.83, 72: 0.83, 73: 0.83, 74: 0.83, 75: 0.83, 76: 0.74, 77: 0.74, 78: 0.74,
                  79: 0.74, 80: 0.74, 81: 0.74, 82: 0.74, 83: 0.74, 84: 0.74, 85: 0.74, 86: 0.74,
                  87: 0.74, 88: 0.74, 89: 0.74, 90: 0.74, 91: 0.74, 92: 0.74, 93: 0.74, 94: 0.74,
                  95: 0.74, 96: 0.74, 97: 0.74, 98: 0.74, 99: 0.74, 100: 0.74, 101: 0.74,
                  102: 0.74, 103: 0.74, 104: 0.74, 105: 0.74, 106: 0.74, 107: 0.74, 108: 0.74,
                  109: 0.74, 110: 0.74}}

################################################################
# Valores de las canastas basicas alimentaria y total en pesos
# para un varon adulto en argentina. La serie intentara actualizarse
# a medida que surjan nuevos datos. Fuente indec a traves de https://datos.gob.ar/dataset
CBA = {
    datetime.datetime(2000,9,1): {1: 62.44, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2000,10,1): {1: 64.3, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2000,11,1): {1: 63.47, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2000,12,1): {1: 62.76, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,1,1): {1: 62.73, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,2,1): {1: 62.73, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,3,1): {1: 62.92, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,4,1): {1: 63.24, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,5,1): {1: 62.74, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,6,1): {1: 61.76, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,7,1): {1: 61.59, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,8,1): {1: 61.37, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,9,1): {1: 61.02, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,10,1): {1: 60.5, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,11,1): {1: 60.75, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,12,1): {1: 60.46, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,1,1): {1: 62.41, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,2,1): {1: 65.82, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,3,1): {1: 69.83, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,4,1): {1: 81.76, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,5,1): {1: 86.2, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,6,1): {1: 90.67, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,7,1): {1: 94.93, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,8,1): {1: 100.94, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,9,1): {1: 104.87, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,10,1): {1: 103.74, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,11,1): {1: 105.08, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,12,1): {1: 105.72, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,1,1): {1: 106.92, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,2,1): {1: 107.56, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,3,1): {1: 107.83, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,4,1): {1: 106.55, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,5,1): {1: 104.6, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,6,1): {1: 103.13, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,7,1): {1: 102.31, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,8,1): {1: 102.08, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,9,1): {1: 101.99, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,10,1): {1: 104.12, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,11,1): {1: 105.24, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,12,1): {1: 105.76, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,1,1): {1: 105.81, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,2,1): {1: 106.17, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,3,1): {1: 106.02, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,4,1): {1: 106.52, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,5,1): {1: 106.66, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,6,1): {1: 106.88, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,7,1): {1: 106.14, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,8,1): {1: 107.9, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,9,1): {1: 108.54, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,10,1): {1: 108.1, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,11,1): {1: 108.25, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,12,1): {1: 108.36, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,1,1): {1: 108.66, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,2,1): {1: 111.37, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,3,1): {1: 114.71, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,4,1): {1: 114.18, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,5,1): {1: 114.04, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,6,1): {1: 114.49, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,7,1): {1: 115.78, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,8,1): {1: 117.5, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,9,1): {1: 120.14, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,10,1): {1: 120.7, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,11,1): {1: 124.73, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,12,1): {1: 124.59, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,1,1): {1: 125.81, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,2,1): {1: 127.19, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,3,1): {1: 129.44, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,4,1): {1: 128.49, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,5,1): {1: 126.57, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,6,1): {1: 126.62, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,7,1): {1: 126.36, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,8,1): {1: 126.68, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,9,1): {1: 126.78, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,10,1): {1: 127.88, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,11,1): {1: 130.5, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,12,1): {1: 134.14, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,1,1): {1: 137.62, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,2,1): {1: 138.73, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,3,1): {1: 138.45, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,4,1): {1: 138.58, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,5,1): {1: 138.58, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,6,1): {1: 139, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,7,1): {1: 139.98, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,8,1): {1: 143.05, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,9,1): {1: 144.01, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,10,1): {1: 144.79, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,11,1): {1: 142.83, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,12,1): {1: 143.1, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,1,1): {1: 144.21, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,2,1): {1: 145.5, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,3,1): {1: 147.43, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,4,1): {1: 148.09, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,5,1): {1: 145.62, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,6,1): {1: 147.11, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,7,1): {1: 143.43, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,8,1): {1: 142.04, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,9,1): {1: 142.51, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,10,1): {1: 143.1, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,11,1): {1: 143.59, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,12,1): {1: 143.7, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,1,1): {1: 143.47, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,2,1): {1: 142.96, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,3,1): {1: 143.54, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,4,1): {1: 143.8, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,5,1): {1: 143.63, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,6,1): {1: 144.16, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,7,1): {1: 144.78, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,8,1): {1: 146.71, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,9,1): {1: 148.22, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,10,1): {1: 150.13, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,11,1): {1: 152.03, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,12,1): {1: 155.94, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,1,1): {1: 159.3, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,2,1): {1: 166.86, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,3,1): {1: 170.94, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,4,1): {1: 172.04, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,5,1): {1: 172.24, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,6,1): {1: 173.74, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,7,1): {1: 174.64, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,8,1): {1: 176.58, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,9,1): {1: 178.25, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,10,1): {1: 184.39, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,11,1): {1: 186.45, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,12,1): {1: 187.24, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,1,1): {1: 187.75, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,2,1): {1: 187.79, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,3,1): {1: 189.49, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,4,1): {1: 191.05, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,5,1): {1: 192.08, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,6,1): {1: 192.93, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,7,1): {1: 195.26, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,8,1): {1: 197.27, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,9,1): {1: 199.56, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,10,1): {1: 201.37, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,11,1): {1: 203.02, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,12,1): {1: 205.31, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,1,1): {1: 207.33, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,2,1): {1: 209.98, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,3,1): {1: 213.42, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,4,1): {1: 216.26, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,5,1): {1: 218.26, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,6,1): {1: 220.6, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,7,1): {1: 222.77, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,8,1): {1: 226.22, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,9,1): {1: 229.01, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,10,1): {1: 230.75, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,11,1): {1: 231.86, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,12,1): {1: 232.71, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,1,1): {1: 233.13, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,2,1): {1: 233.47, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,3,1): {1: 234.63, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,4,1): {1: 234.94, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,5,1): {1: 234.54, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,6,1): {1: 236.58, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,7,1): {1: 239.80, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,8,1): {1: 242.49, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,9,1): {1: 242.90, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,10,1): {1: 245.75, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,11,1): {1: 249.06, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,12,1): {1: 254.78, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2014,1,1): {1: 751.3, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2014,2,1): {1: 822.2, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2014,3,1): {1: 852.8, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2014,4,1): {1: 873.9, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,5,1): {1: 1075.3, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,6,1): {1: 1087.1, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,7,1): {1: 1116.3, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,8,1): {1: 1153.1, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,9,1): {1: 1163.5, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,10,1): {1: 1160.8, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,11,1): {1: 1190.4, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,12,1): {1: 1273.9, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2016,1,1): {1: 1312.7, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2016,2,1): {1: 1350.5, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2016,3,1): {1: 1424.9, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2016,4,1): {1: 1514.53, 42: 1358.29, 41: 1371.62, 40: 1333.91, 43: 1514.96, 44: 1556.96},
    datetime.datetime(2016,5,1): {1: 1561.35, 42: 1398.2, 41: 1404.27, 40: 1369.37, 43: 1562.75, 44: 1604.3},
    datetime.datetime(2016,6,1): {1: 1614.32, 42: 1445.22, 41: 1447.63, 40: 1413.77, 43: 1614.33, 44: 1661.42},
    datetime.datetime(2016,7,1): {1: 1666.48, 42: 1494.04, 41: 1496.21, 40: 1458.24, 43: 1660.19, 44: 1713.67},
    datetime.datetime(2016,8,1): {1: 1675.05, 42: 1496.09, 41: 1501.91, 40: 1459.38, 43: 1662.99, 44: 1723.86},
    datetime.datetime(2016,9,1): {1: 1711.22, 42: 1536.29, 41: 1540.68, 40: 1498.62, 43: 1705.31, 44: 1767.89},
    datetime.datetime(2016,10,1): {1: 1739.34, 42: 1559.0, 41: 1559.51, 40: 1516.82, 43: 1731.84, 44: 1797.44},
    datetime.datetime(2016,11,1): {1: 1762.65, 42: 1577.67, 41: 1573.18, 40: 1532.67, 43: 1753.27, 44: 1819.64},
    datetime.datetime(2016,12,1): {1: 1766.62, 42: 1574.22, 41: 1571.59, 40: 1526.86, 43: 1754.08, 44: 1822.96},
    datetime.datetime(2017,1,1): {1: 1789.15, 42: 1595.31, 41: 1590.44, 40: 1541.73, 43: 1777.09, 44: 1848.5},
    datetime.datetime(2017,2,1): {1: 1821.02, 42: 1628.44, 41: 1620.35, 40: 1570.68, 43: 1810.32, 44: 1882.42},
    datetime.datetime(2017,3,1): {1: 1876.56, 42: 1681.49, 41: 1672.78, 40: 1623.37, 43: 1863.58, 44: 1939.76},
    datetime.datetime(2017,4,1): {1: 1915.47, 42: 1713.91, 41: 1709.08, 40: 1661.21, 43: 1901.16, 44: 1975.67},
    datetime.datetime(2017,5,1): {1: 1937.24, 42: 1731.7, 41: 1729.4, 40: 1681.95, 43: 1922.0, 44: 1994.94},
    datetime.datetime(2017,6,1): {1: 1956.42, 42: 1747.95, 41: 1743.77, 40: 1698.68, 43: 1940.78, 44: 2012.13},
    datetime.datetime(2017,7,1): {1: 1984.64, 42: 1767.82, 41: 1761.4, 40: 1718.31, 43: 1965.17, 44: 2034.9},
    datetime.datetime(2017,8,1): {1: 2021.98, 42: 1805.99, 41: 1796.81, 40: 1756.04, 43: 2007.97, 44: 2074.38},
    datetime.datetime(2017,9,1): {1: 2049.39, 42: 1830.78, 41: 1821.54, 40: 1783.48, 43: 2037.82, 44: 2101.72},
    datetime.datetime(2017,10,1): {1: 2079.23, 42: 1860.27, 41: 1851.72, 40: 1812.72, 43: 2071.1, 44: 2137.17},
    datetime.datetime(2017,11,1): {1: 2125.84, 42: 1900.71, 41: 1895.81, 40: 1852.43, 43: 2117.11, 44: 2192.85},
    datetime.datetime(2017,12,1): {1: 2150.29, 42: 1920.79, 41: 1917.87, 40: 1870.58, 43: 2141.51, 44: 2221.58},
    datetime.datetime(2018,1,1): {1: 2197.26, 42: 1966.81, 41: 1960.18, 40: 1915.2, 43: 2192.22, 44: 2271.63},
    datetime.datetime(2018,2,1): {1: 2261.23, 42: 2022.41, 41: 2018.25, 40: 1969.14, 43: 2251.12, 44: 2343.56},
    datetime.datetime(2018,3,1): {1: 2294.56, 42: 2048.42, 41: 2047.14, 40: 1996.75, 43: 2277.22, 44: 2367.1},
    datetime.datetime(2018,4,1): {1: 2308.11, 42: 2054.73, 41: 2054.75, 40: 2000.01, 43: 2284.59, 44: 2373.68},
    datetime.datetime(2018,5,1): {1: 2418.65, 42: 2153.78, 41: 2153.6, 40: 2088.53, 43: 2389.72, 44: 2493.43},
    datetime.datetime(2018,6,1): {1: 2537.45, 42: 2265.25, 41: 2262.69, 40: 2199.69, 43: 2508.45, 44: 2615.63},
    datetime.datetime(2018,7,1):  {1: 2627.37, 40: 2292.29, 41: 2349.38, 42: 2355.98, 43: 2606.91, 44: 2710.54},
    datetime.datetime(2018,8,1):  {1: 2701.48, 40: 2361.61, 41: 2414.60, 42: 2421.94, 43: 2683.06, 44: 2782.76},
    datetime.datetime(2018,9,1):  {1: 2931.88, 40: 2557.36, 41: 2620.84, 42: 2622.96, 43: 2901.79, 44: 3015.31},
    datetime.datetime(2018,10,1): {1: 3150.62, 40: 2738.89, 41: 2810.69, 42: 2819.87, 43: 3119.33, 44: 3242.01},
    datetime.datetime(2018,11,1): {1: 3276.02, 40: 2844.81, 41: 2922.84, 42: 2934.83, 43: 3248.55, 44: 3387.27},
    datetime.datetime(2018,12,1): {1: 3300.17, 40: 2852.75, 41: 2925.50, 42: 2942.70, 43: 3269.58, 44: 3393.87},
    datetime.datetime(2019,1,1):  {1: 3423.03, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan}, 
    datetime.datetime(2019,2,1):  {1: 3597.77,40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan}}

#########################################
### canasta basica total
CBT = {
    datetime.datetime(2000,9,1): {1: 151.1, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2000,10,1): {1: 154.95, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2000,11,1): {1: 154.23, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2000,12,1): {1: 153.75, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,1,1): {1: 153.69, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,2,1): {1: 153.69, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,3,1): {1: 153.52, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,4,1): {1: 154.3, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,5,1): {1: 153.1, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,6,1): {1: 151.93, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,7,1): {1: 151.51, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,8,1): {1: 150.96, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,9,1): {1: 150.11, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,10,1): {1: 150.05, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,11,1): {1: 150.05, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2001,12,1): {1: 149.32, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,1,1): {1: 154.16, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,2,1): {1: 161.26, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,3,1): {1: 169.7, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,4,1): {1: 193.77, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,5,1): {1: 202.57, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,6,1): {1: 210.36, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,7,1): {1: 218.34, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,8,1): {1: 227.1, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,9,1): {1: 231.77, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,10,1): {1: 230.31, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,11,1): {1: 232.23, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2002,12,1): {1: 232.59, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,1,1): {1: 235.22, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,2,1): {1: 235.56, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,3,1): {1: 233.99, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,4,1): {1: 232.28, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,5,1): {1: 229.07, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,6,1): {1: 227.92, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,7,1): {1: 227.13, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,8,1): {1: 225.6, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,9,1): {1: 224.38, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,10,1): {1: 228.02, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,11,1): {1: 229.42, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2003,12,1): {1: 231.61, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,1,1): {1: 231.72, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,2,1): {1: 232.51, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,3,1): {1: 232.18, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,4,1): {1: 233.29, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,5,1): {1: 233.58, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,6,1): {1: 234.08, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,7,1): {1: 234.57, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,8,1): {1: 236.3, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,9,1): {1: 237.7, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,10,1): {1: 237.82, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,11,1): {1: 238.15, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2004,12,1): {1: 239.48, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,1,1): {1: 241.23, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,2,1): {1: 246.13, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,3,1): {1: 250.07, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,4,1): {1: 250.05, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,5,1): {1: 249.75, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,6,1): {1: 251.88, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,7,1): {1: 254.72, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,8,1): {1: 256.15, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,9,1): {1: 259.49, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,10,1): {1: 261.92, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,11,1): {1: 268.17, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2005,12,1): {1: 269.11, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,1,1): {1: 273.01, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,2,1): {1: 274.73, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,3,1): {1: 278.3, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,4,1): {1: 277.54, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,5,1): {1: 275.92, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,6,1): {1: 277.3, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,7,1): {1: 277.99, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,8,1): {1: 278.7, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,9,1): {1: 278.92, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,10,1): {1: 281.34, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,11,1): {1: 284.49, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2006,12,1): {1: 291.08, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,1,1): {1: 295.89, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,2,1): {1: 298.27, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,3,1): {1: 296.28, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,4,1): {1: 296.57, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,5,1): {1: 297.96, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,6,1): {1: 298.86, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,7,1): {1: 300.95, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,8,1): {1: 306.13, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,9,1): {1: 308.19, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,10,1): {1: 311.29, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,11,1): {1: 307.09, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2007,12,1): {1: 309.1, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,1,1): {1: 313.01, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,2,1): {1: 314.19, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,3,1): {1: 317.92, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,4,1): {1: 319.44, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,5,1): {1: 315.99, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,6,1): {1: 319.86, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,7,1): {1: 315.67, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,8,1): {1: 312.63, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,9,1): {1: 313.69, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,10,1): {1: 314.96, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,11,1): {1: 316.69, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2008,12,1): {1: 317.1, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,1,1): {1: 319.84, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,2,1): {1: 320.7, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,3,1): {1: 322.57, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,4,1): {1: 323.93, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,5,1): {1: 324.19, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,6,1): {1: 325.99, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,7,1): {1: 327.94, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,8,1): {1: 331.76, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,9,1): {1: 334.45, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,10,1): {1: 338.38, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,11,1): {1: 341.78, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2009,12,1): {1: 348.67, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,1,1): {1: 354.46, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,2,1): {1: 366.12, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,3,1): {1: 371.63, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,4,1): {1: 372.33, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,5,1): {1: 373.37, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,6,1): {1: 377.17, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,7,1): {1: 380.09, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,8,1): {1: 383.99, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,9,1): {1: 387.66, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,10,1): {1: 398.55, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,11,1): {1: 402.63, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2010,12,1): {1: 405.35, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,1,1): {1: 409.46, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,2,1): {1: 411.49, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,3,1): {1: 415.44, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,4,1): {1: 419.3, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,5,1): {1: 423.04, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,6,1): {1: 425.3, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,7,1): {1: 430.9, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,8,1): {1: 436.03, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,9,1): {1: 441.01, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,10,1): {1: 444.27, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,11,1): {1: 448.51, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2011,12,1): {1: 454.49, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,1,1): {1: 460.82, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,2,1): {1: 465.34, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,3,1): {1: 472.13, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,4,1): {1: 477.21, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,5,1): {1: 482.67, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,6,1): {1: 487.85, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,7,1): {1: 494.68, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,8,1): {1: 503.37, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,9,1): {1: 509.97, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,10,1): {1: 513.79, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,11,1): {1: 517.85, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2012,12,1): {1: 522.14, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,1,1): {1: 529.31, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,2,1): {1: 530.44, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,3,1): {1: 533.93, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,4,1): {1: 535.73, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,5,1): {1: 534.81, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,6,1): {1: 543.48, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,7,1): {1: 550.05, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,8,1): {1: 552.88, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,9,1): {1: 555.94, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,10,1): {1: 561.28, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,11,1): {1: 566.43, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2013,12,1): {1: 577.23, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2014,1,1): {1: 1833.2, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2014,2,1): {1: 2006.2, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2014,3,1): {1: 2080.9, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2014,4,1): {1: 2132.4, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,5,1): {1: 2404.7, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,6,1): {1: 2415.1, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,7,1): {1: 2493.4, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,8,1): {1: 2597.7, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,9,1): {1: 2609.7, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,10,1): {1: 2565.6, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,11,1): {1: 2640.3, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2015,12,1): {1: 2887.2, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2016,1,1): {1: 2964.1, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2016,2,1): {1: 3026.1, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2016,3,1): {1: 3177.2, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan},
    datetime.datetime(2016,4,1): {1: 3665.17, 42: 3504.4, 41: 3099.86, 40: 2987.95, 43: 3666.21, 44: 4281.63},
    datetime.datetime(2016,5,1): {1: 3825.3, 42: 3649.31, 41: 3229.82, 40: 3122.17, 43: 3828.74, 44: 4459.96},
    datetime.datetime(2016,6,1): {1: 3938.94, 42: 3757.58, 41: 3315.07, 40: 3209.27, 43: 3938.96, 44: 4602.13},
    datetime.datetime(2016,7,1): {1: 4032.88, 42: 3854.62, 41: 3396.4, 40: 3281.04, 43: 4017.66, 44: 4712.59},
    datetime.datetime(2016,8,1): {1: 4036.87, 42: 3844.95, 41: 3394.32, 40: 3269.01, 43: 4007.81, 44: 4723.38},
    datetime.datetime(2016,9,1): {1: 4089.82, 42: 3917.54, 41: 3451.12, 40: 3326.94, 43: 4075.69, 44: 4790.98},
    datetime.datetime(2016,10,1): {1: 4191.81, 42: 4006.63, 41: 3524.49, 40: 3397.68, 43: 4173.73, 44: 4924.99},
    datetime.datetime(2016,11,1): {1: 4247.99, 42: 4038.84, 41: 3539.66, 40: 3417.85, 43: 4225.38, 44: 4967.62},
    datetime.datetime(2016,12,1): {1: 4257.55, 42: 4045.75, 41: 3551.79, 40: 3420.17, 43: 4227.33, 44: 4994.91},
    datetime.datetime(2017,1,1): {1: 4311.85, 42: 4099.95, 41: 3594.39, 40: 3453.48, 43: 4282.79, 44: 5064.89},
    datetime.datetime(2017,2,1): {1: 4425.08, 42: 4217.66, 41: 3694.4, 40: 3549.74, 43: 4399.08, 44: 5195.48},
    datetime.datetime(2017,3,1): {1: 4560.04, 42: 4338.24, 41: 3797.21, 40: 3652.58, 43: 4528.5, 44: 5334.34},
    datetime.datetime(2017,4,1): {1: 4692.9, 42: 4456.17, 41: 3913.79, 40: 3770.95, 43: 4638.83, 44: 5492.36},
    datetime.datetime(2017,5,1): {1: 4746.24, 42: 4519.74, 41: 3960.33, 40: 3818.03, 43: 4708.9, 44: 5545.93},
    datetime.datetime(2017,6,1): {1: 4793.23, 42: 4562.15, 41: 4010.67, 40: 3872.99, 43: 4754.91, 44: 5613.84},
    datetime.datetime(2017,7,1): {1: 4862.37, 42: 4614.01, 41: 4051.22, 40: 3917.75, 43: 4814.67, 44: 5657.02},
    datetime.datetime(2017,8,1): {1: 4933.63, 42: 4695.57, 41: 4096.73, 40: 3968.65, 43: 4899.45, 44: 5746.03},
    datetime.datetime(2017,9,1): {1: 5000.51, 42: 4760.03, 41: 4171.33, 40: 4048.5, 43: 4972.28, 44: 5821.76},
    datetime.datetime(2017,10,1): {1: 5073.32, 42: 4836.7, 41: 4240.44, 40: 4114.87, 43: 5053.48, 44: 5919.96},
    datetime.datetime(2017,11,1): {1: 5187.05, 42: 4941.85, 41: 4341.4, 40: 4205.02, 43: 5165.75, 44: 6074.19},
    datetime.datetime(2017,12,1): {1: 5397.23, 42: 5147.72, 41: 4506.99, 40: 4358.45, 43: 5375.19, 44: 6331.5},
    datetime.datetime(2018,1,1): {1: 5493.15, 42: 5231.71, 41: 4586.82, 40: 4443.29, 43: 5480.55, 44: 6428.74},
    datetime.datetime(2018,2,1): {1: 5675.69, 42: 5399.83, 41: 4742.91, 40: 4588.12, 43: 5650.34, 44: 6679.15},
    datetime.datetime(2018,3,1): {1: 5782.29, 42: 5489.79, 41: 4831.25, 40: 4672.42, 43: 5738.59, 44: 6769.93},
    datetime.datetime(2018,4,1): {1: 5908.76, 42: 5588.89, 41: 4910.88, 40: 4740.02, 43: 5848.55, 44: 6883.7},
    datetime.datetime(2018,5,1): {1: 6095.0, 42: 5772.13, 41: 5082.5, 40: 4887.18, 43: 6022.12, 44: 7131.24},
    datetime.datetime(2018,6,1): {1: 6343.63, 42: 6025.59, 41: 5294.72, 40: 5103.28, 43: 6271.13, 44: 7428.39},
    datetime.datetime(2018,7,1):  {1: 6515.88, 40: 5272.27, 41: 5450.56, 42:6219.79, 43: 6465.14, 44: 7616.62},
    datetime.datetime(2018,8,1):  {1: 6753.70, 40: 5478.94, 41: 5650.16, 42: 6442.36, 43: 6707.65, 44: 7903.04},
    datetime.datetime(2018,9,1):  {1: 7300.38, 40: 5907.50, 41: 6106.56, 42: 6950.84, 43: 7225.46, 44: 8533.33},
    datetime.datetime(2018,10,1): {1: 7845.04, 40: 6326.84, 41: 6548.91, 42: 7472.66, 43: 7767.13, 44: 9142.47},
    datetime.datetime(2018,11,1): {1: 8157.29, 40: 6571.51, 41: 6810.22, 42: 7777.30, 43: 8088.89, 44: 9552.10},
    datetime.datetime(2018,12,1): {1: 8250.42, 40: 6618.38, 41: 6854.67, 42: 7827.58, 43: 8173.95, 44: 9638.59},
    datetime.datetime(2019,1,1):  {1: 8557.58, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan}, 
    datetime.datetime(2019,2,1):  {1: 8922.47, 40: np.nan, 41: np.nan, 42: np.nan, 43: np.nan, 44: np.nan}}

###################################################
### evolucion del indice ripte (remuneracion, imponible
### promedio de los trabajadores estatales?)
ripte = {
  datetime.datetime(1994,7,1): 874.87,
  datetime.datetime(1994,8,1): 893.0,
  datetime.datetime(1994,9,1): 907.99,
  datetime.datetime(1994,10,1): 910.75,
  datetime.datetime(1994,11,1): 916.93,
  datetime.datetime(1994,12,1): 936.83,
  datetime.datetime(1995,1,1): 934.85,
  datetime.datetime(1995,2,1): 928.29,
  datetime.datetime(1995,3,1): 931.37,
  datetime.datetime(1995,4,1): 909.07,
  datetime.datetime(1995,5,1): 920.51,
  datetime.datetime(1995,6,1): 944.4,
  datetime.datetime(1995,7,1): 914.41,
  datetime.datetime(1995,8,1): 912.86,
  datetime.datetime(1995,9,1): 914.69,
  datetime.datetime(1995,10,1): 916.48,
  datetime.datetime(1995,11,1): 921.41,
  datetime.datetime(1995,12,1): 938.88,
  datetime.datetime(1996,1,1): 938.19,
  datetime.datetime(1996,2,1): 933.47,
  datetime.datetime(1996,3,1): 932.13,
  datetime.datetime(1996,4,1): 924.13,
  datetime.datetime(1996,5,1): 929.27,
  datetime.datetime(1996,6,1): 940.95,
  datetime.datetime(1996,7,1): 930.16,
  datetime.datetime(1996,8,1): 936.68,
  datetime.datetime(1996,9,1): 939.25,
  datetime.datetime(1996,10,1): 940.12,
  datetime.datetime(1996,11,1): 921.9,
  datetime.datetime(1996,12,1): 940.06,
  datetime.datetime(1997,1,1): 923.49,
  datetime.datetime(1997,2,1): 923.58,
  datetime.datetime(1997,3,1): 919.47,
  datetime.datetime(1997,4,1): 912.02,
  datetime.datetime(1997,5,1): 914.59,
  datetime.datetime(1997,6,1): 928.71,
  datetime.datetime(1997,7,1): 905.95,
  datetime.datetime(1997,8,1): 898.67,
  datetime.datetime(1997,9,1): 910.27,
  datetime.datetime(1997,10,1): 913.02,
  datetime.datetime(1997,11,1): 899.56,
  datetime.datetime(1997,12,1): 915.57,
  datetime.datetime(1998,1,1): 912.48,
  datetime.datetime(1998,2,1): 908.13,
  datetime.datetime(1998,3,1): 910.79,
  datetime.datetime(1998,4,1): 899.86,
  datetime.datetime(1998,5,1): 896.37,
  datetime.datetime(1998,6,1): 920.38,
  datetime.datetime(1998,7,1): 898.35,
  datetime.datetime(1998,8,1): 888.71,
  datetime.datetime(1998,9,1): 894.03,
  datetime.datetime(1998,10,1): 892.2,
  datetime.datetime(1998,11,1): 891.18,
  datetime.datetime(1998,12,1): 905.15,
  datetime.datetime(1999,1,1): 890.77,
  datetime.datetime(1999,2,1): 897.38,
  datetime.datetime(1999,3,1): 896.72,
  datetime.datetime(1999,4,1): 884.93,
  datetime.datetime(1999,5,1): 883.81,
  datetime.datetime(1999,6,1): 898.59,
  datetime.datetime(1999,7,1): 888.23,
  datetime.datetime(1999,8,1): 883.4,
  datetime.datetime(1999,9,1): 886.14,
  datetime.datetime(1999,10,1): 888.3,
  datetime.datetime(1999,11,1): 887.21,
  datetime.datetime(1999,12,1): 892.14,
  datetime.datetime(2000,1,1): 888.29,
  datetime.datetime(2000,2,1): 893.47,
  datetime.datetime(2000,3,1): 889.13,
  datetime.datetime(2000,4,1): 871.69,
  datetime.datetime(2000,5,1): 878.6,
  datetime.datetime(2000,6,1): 903.02,
  datetime.datetime(2000,7,1): 882.35,
  datetime.datetime(2000,8,1): 881.81,
  datetime.datetime(2000,9,1): 883.15,
  datetime.datetime(2000,10,1): 879.95,
  datetime.datetime(2000,11,1): 877.91,
  datetime.datetime(2000,12,1): 884.83,
  datetime.datetime(2001,1,1): 887.75,
  datetime.datetime(2001,2,1): 892.27,
  datetime.datetime(2001,3,1): 892.12,
  datetime.datetime(2001,4,1): 884.92,
  datetime.datetime(2001,5,1): 882.57,
  datetime.datetime(2001,6,1): 889.09,
  datetime.datetime(2001,7,1): 881.29,
  datetime.datetime(2001,8,1): 879.63,
  datetime.datetime(2001,9,1): 876.4,
  datetime.datetime(2001,10,1): 880.26,
  datetime.datetime(2001,11,1): 875.83,
  datetime.datetime(2001,12,1): 870.52,
  datetime.datetime(2002,1,1): 875.77,
  datetime.datetime(2002,2,1): 887.02,
  datetime.datetime(2002,3,1): 882.39,
  datetime.datetime(2002,4,1): 882.13,
  datetime.datetime(2002,5,1): 882.12,
  datetime.datetime(2002,6,1): 874.52,
  datetime.datetime(2002,7,1): 906.76,
  datetime.datetime(2002,8,1): 885.3,
  datetime.datetime(2002,9,1): 891.87,
  datetime.datetime(2002,10,1): 900.78,
  datetime.datetime(2002,11,1): 894.65,
  datetime.datetime(2002,12,1): 900.69,
  datetime.datetime(2003,1,1): 882.03,
  datetime.datetime(2003,2,1): 890.6,
  datetime.datetime(2003,3,1): 892.09,
  datetime.datetime(2003,4,1): 889.92,
  datetime.datetime(2003,5,1): 887.23,
  datetime.datetime(2003,6,1): 903.82,
  datetime.datetime(2003,7,1): 918.96,
  datetime.datetime(2003,8,1): 941.34,
  datetime.datetime(2003,9,1): 971.97,
  datetime.datetime(2003,10,1): 1008.52,
  datetime.datetime(2003,11,1): 1020.36,
  datetime.datetime(2003,12,1): 1040.73,
  datetime.datetime(2004,1,1): 1065.01,
  datetime.datetime(2004,2,1): 1090.92,
  datetime.datetime(2004,3,1): 1102.42,
  datetime.datetime(2004,4,1): 1099.03,
  datetime.datetime(2004,5,1): 1088.57,
  datetime.datetime(2004,6,1): 1100.62,
  datetime.datetime(2004,7,1): 1087.79,
  datetime.datetime(2004,8,1): 1085.02,
  datetime.datetime(2004,9,1): 1083.64,
  datetime.datetime(2004,10,1): 1088.43,
  datetime.datetime(2004,11,1): 1090.41,
  datetime.datetime(2004,12,1): 1102.69,
  datetime.datetime(2005,1,1): 1101.82,
  datetime.datetime(2005,2,1): 1102.37,
  datetime.datetime(2005,3,1): 1113.64,
  datetime.datetime(2005,4,1): 1156.06,
  datetime.datetime(2005,5,1): 1170.58,
  datetime.datetime(2005,6,1): 1196.59,
  datetime.datetime(2005,7,1): 1231.34,
  datetime.datetime(2005,8,1): 1277.97,
  datetime.datetime(2005,9,1): 1304.53,
  datetime.datetime(2005,10,1): 1352.94,
  datetime.datetime(2005,11,1): 1366.43,
  datetime.datetime(2005,12,1): 1371.54,
  datetime.datetime(2006,1,1): 1388.13,
  datetime.datetime(2006,2,1): 1407.77,
  datetime.datetime(2006,3,1): 1441.65,
  datetime.datetime(2006,4,1): 1465.06,
  datetime.datetime(2006,5,1): 1502.34,
  datetime.datetime(2006,6,1): 1527.82,
  datetime.datetime(2006,7,1): 1554.83,
  datetime.datetime(2006,8,1): 1583.35,
  datetime.datetime(2006,9,1): 1596.46,
  datetime.datetime(2006,10,1): 1634.32,
  datetime.datetime(2006,11,1): 1641.74,
  datetime.datetime(2006,12,1): 1672.88,
  datetime.datetime(2007,1,1): 1693.86,
  datetime.datetime(2007,2,1): 1728.39,
  datetime.datetime(2007,3,1): 1764.52,
  datetime.datetime(2007,4,1): 1815.39,
  datetime.datetime(2007,5,1): 1830.07,
  datetime.datetime(2007,6,1): 1837.71,
  datetime.datetime(2007,7,1): 1896.64,
  datetime.datetime(2007,8,1): 1941.51,
  datetime.datetime(2007,9,1): 1946.53,
  datetime.datetime(2007,10,1): 2019.04,
  datetime.datetime(2007,11,1): 2020.25,
  datetime.datetime(2007,12,1): 2042.46,
  datetime.datetime(2008,1,1): 2050.27,
  datetime.datetime(2008,2,1): 2130.98,
  datetime.datetime(2008,3,1): 2190.53,
  datetime.datetime(2008,4,1): 2336.11,
  datetime.datetime(2008,5,1): 2383.41,
  datetime.datetime(2008,6,1): 2400.12,
  datetime.datetime(2008,7,1): 2485.91,
  datetime.datetime(2008,8,1): 2530.74,
  datetime.datetime(2008,9,1): 2585.35,
  datetime.datetime(2008,10,1): 2628.32,
  datetime.datetime(2008,11,1): 2592.17,
  datetime.datetime(2008,12,1): 2625.44,
  datetime.datetime(2009,1,1): 2594.54,
  datetime.datetime(2009,2,1): 2578.64,
  datetime.datetime(2009,3,1): 2681.73,
  datetime.datetime(2009,4,1): 2731.19,
  datetime.datetime(2009,5,1): 2698.39,
  datetime.datetime(2009,6,1): 2780.38,
  datetime.datetime(2009,7,1): 2803.3,
  datetime.datetime(2009,8,1): 2829.14,
  datetime.datetime(2009,9,1): 2752.01,
  datetime.datetime(2009,10,1): 2795.21,
  datetime.datetime(2009,11,1): 2789.73,
  datetime.datetime(2009,12,1): 3015.95,
  datetime.datetime(2010,1,1): 3011.99,
  datetime.datetime(2010,2,1): 3110.52,
  datetime.datetime(2010,3,1): 3233.2,
  datetime.datetime(2010,4,1): 3305.06,
  datetime.datetime(2010,5,1): 3340.58,
  datetime.datetime(2010,6,1): 3399.89,
  datetime.datetime(2010,7,1): 3485.26,
  datetime.datetime(2010,8,1): 3570.19,
  datetime.datetime(2010,9,1): 3719.15,
  datetime.datetime(2010,10,1): 3795.5,
  datetime.datetime(2010,11,1): 3842.72,
  datetime.datetime(2010,12,1): 3885.52,
  datetime.datetime(2011,1,1): 3935.3,
  datetime.datetime(2011,2,1): 4017.23,
  datetime.datetime(2011,3,1): 4289.54,
  datetime.datetime(2011,4,1): 4343.04,
  datetime.datetime(2011,5,1): 4510.43,
  datetime.datetime(2011,6,1): 4575.38,
  datetime.datetime(2011,7,1): 4718.9,
  datetime.datetime(2011,8,1): 4875.01,
  datetime.datetime(2011,9,1): 5034.58,
  datetime.datetime(2011,10,1): 5115.77,
  datetime.datetime(2011,11,1): 5242.9,
  datetime.datetime(2011,12,1): 5280.28,
  datetime.datetime(2012,1,1): 5350.76,
  datetime.datetime(2012,2,1): 5501.32,
  datetime.datetime(2012,3,1): 5711.74,
  datetime.datetime(2012,4,1): 5983.14,
  datetime.datetime(2012,5,1): 6163.78,
  datetime.datetime(2012,6,1): 6193.16,
  datetime.datetime(2012,7,1): 6413.3,
  datetime.datetime(2012,8,1): 6468.61,
  datetime.datetime(2012,9,1): 6577.58,
  datetime.datetime(2012,10,1): 6743.8,
  datetime.datetime(2012,11,1): 6907.3,
  datetime.datetime(2012,12,1): 6985.82,
  datetime.datetime(2013,1,1): 7063.81,
  datetime.datetime(2013,2,1): 7300.21,
  datetime.datetime(2013,3,1): 7490.7,
  datetime.datetime(2013,4,1): 7634.27,
  datetime.datetime(2013,5,1): 7896.3,
  datetime.datetime(2013,6,1): 7985.95,
  datetime.datetime(2013,7,1): 8176.79,
  datetime.datetime(2013,8,1): 8255.89,
  datetime.datetime(2013,9,1): 8439.6,
  datetime.datetime(2013,10,1): 8626.56,
  datetime.datetime(2013,11,1): 8666.72,
  datetime.datetime(2013,12,1): 8743.73,
  datetime.datetime(2014,1,1): 8785.03,
  datetime.datetime(2014,2,1): 9249.21,
  datetime.datetime(2014,3,1): 9634.75,
  datetime.datetime(2014,4,1): 10156.89,
  datetime.datetime(2014,5,1): 10295.0,
  datetime.datetime(2014,6,1): 10394.2,
  datetime.datetime(2014,7,1): 10894.91,
  datetime.datetime(2014,8,1): 10946.2,
  datetime.datetime(2014,9,1): 11393.26,
  datetime.datetime(2014,10,1): 11730.67,
  datetime.datetime(2014,11,1): 11709.66,
    datetime.datetime(2014,12,1): 11953.5,
    datetime.datetime(2015,1,1): 11997.96,
    datetime.datetime(2015,2,1): 12410.76,
    datetime.datetime(2015,3,1): 12869.99,
    datetime.datetime(2015,4,1): 13077.29,
    datetime.datetime(2015,5,1): 13556.91,
    datetime.datetime(2015,6,1): 14104.40,
    datetime.datetime(2015,7,1): 14535.78,
    datetime.datetime(2015,8,1): 14598.39,
    datetime.datetime(2015,9,1): 14983.39,
    datetime.datetime(2015,10,1): 15202.43,
    datetime.datetime(2015,11,1): 15526.15,
    datetime.datetime(2015,12,1): 15800.97,
    datetime.datetime(2016,1,1):  15822.89,
    datetime.datetime(2016,2,1):  16520.52,
    datetime.datetime(2016,3,1):  16977.26,
    datetime.datetime(2016,4,1):  17691.29,
    datetime.datetime(2016,5,1):  18042.71,
    datetime.datetime(2016,6,1):  18277.57,
    datetime.datetime(2016,7,1):  18988.44,
    datetime.datetime(2016,8,1):  19216.78,
    datetime.datetime(2016,9,1):  19666.45,
    datetime.datetime(2016,10,1): 20069.28,
    datetime.datetime(2016,11,1): 20422.65,
    datetime.datetime(2016,12,1): 20690.14,
    datetime.datetime(2017,1,1):  21048.21,
    datetime.datetime(2017,2,1):  21483.03,
    datetime.datetime(2017,3,1):  22285.48,
    datetime.datetime(2017,4,1):  22650.53,
    datetime.datetime(2017,5,1):  23029.98,
    datetime.datetime(2017,6,1):  23469.98,
    datetime.datetime(2017,7,1):  24489.17,
    datetime.datetime(2017,8,1):  24700.42,
    datetime.datetime(2017,9,1):  25136.35,
    datetime.datetime(2017,10,1): 25843.46,
    datetime.datetime(2017,11,1): 26177.33,
    datetime.datetime(2017,12,1): 26301.42,
    datetime.datetime(2018,1,1):  26929.81,
    datetime.datetime(2018,2,1):  27440.22,
    datetime.datetime(2018,3,1):  28072.31,
    datetime.datetime(2018,4,1):  28858.05,
    datetime.datetime(2018,5,1):  29338.79,
    datetime.datetime(2018,6,1):  29598.12,
    datetime.datetime(2018,7,1):  30283.84,
    datetime.datetime(2018,8,1):  30978.75,
    datetime.datetime(2018,9,1):  31523.56,
    datetime.datetime(2018,10,1): 33154.28,
    datetime.datetime(2018,11,1): 33733.80}

###########################################
### Metadata
# Codigos de regiones
region_map = {1: 'Gran Buenos Aires',
              40: 'NOA',
              41: 'NEA',
              42: 'Cuyo',
              43: 'Pampeana',
              44: 'Patagonia'}

###########################################
### Metadata
# Codigos de alglomerados urbanos utilizados en la EPH.
aglo_map = {2: 'Gran La Plata',
            3: 'Bahía Blanca - Cerri',
            4: 'Gran Rosario',
            5: 'Gran Santa Fé',
            6: 'Gran Paraná',
            7: 'Posadas',
            8: 'Gran Resistencia',
            9: 'Cdro. Rivadavia - R.Tilly',
            10: 'Gran Mendoza',
            12: 'Corrientes',
            13: 'Gran Córdoba',
            14: 'Concordia',
            15: 'Formosa',
            17: 'Neuquén – Plottier',
            18: 'S.del Estero - La Banda',
            19: 'Jujuy - Palpalá',
            20: 'Río Gallegos',
            22: 'Gran Catamarca',
            23: 'Salta',
            25: 'La Rioja',
            26: 'San Luis - El Chorrillo',
            27: 'Gran San Juan',
            29: 'Gran Tucumán - T. Viejo',
            30: 'Santa Rosa - Toay',
            31: 'Ushuaia - Río Grande',
            32: 'Ciudad de Bs As',
            33: 'Partidos del GBA',
            34: 'Mar del Plata - Batán',
            36: 'Río Cuarto',
            38: 'San Nicolás – Villa Constitución',
            91: 'Rawson – Trelew',
            93: 'Viedma – Carmen de Patagones'}

############################################
### funciones y utlilidades para trabajar con las tablas
monthMap = { 'Ene': 1, 'Feb': 2, 'Mar': 3, 'Abr': 4, 'May': 5,
             'Jun': 6, 'Jul': 7, 'Ago': 8, 'Sep': 9, 'Set': 9,
             'Oct': 10, 'Nov': 11, 'Dic': 12 }

#####################################
### dates parser for converting from strings to datetime.datetime
def parse_dates(x, d_string= 'Mes'):
    """
    Description:
    this function is used to parse the dates from excel
    to datetime, when the date that is loaded from excel
    spreadsheets is a string such as 'Jul-2008' and so.
    
    parameters:
    ----------
    x: pd.DataFrame or pd.Series,
    dataset loaded from (probably) an excel spreadsheet
    d_string: str,
    the name of the column of the dataset where the dates are stored

    Returns:
    -------
    datetime.datetime,
    returns the date as a datetime.datetime object
    
    """
    
    if isinstance(x[d_string], str):
        m, y = x[d_string].split(sep='-')
        mm = monthMap[m]
        yy = int(y)
        return datetime.datetime(yy, mm, 1)
    
    elif isinstance(x[d_string], datetime.datetime):
        return x[d_string]
    
####################################
### another dates parser for converting string dates to datetime.datetime
def parse_dates2(x, d_string= 'Mes'):
    """
    Description:
    this function is used to parse the dates from excel
    to datetime, when the date that is loaded from excel
    spreadsheets is a string such as 'Jul-2008' and so.
    
    parameters:
    ----------
    x: pd.DataFrame or pd.Series,
    dataset loaded from (probably) an excel spreadsheet
    d_string: str,
    the name of the column of the dataset where the dates are stored

    Returns:
    -------
    datetime.datetime,
    returns the date as a datetime.datetime object
    
    """
    
    if isinstance(x[d_string], str):
        y, m, d = x[d_string].split(sep='-')
        dd = int(d)
        mm = int(m)
        yy = int(y)
        return datetime.datetime(yy, mm, dd)
    
    elif isinstance(x[d_string], datetime.datetime):
        return x[d_string]

##################################
### 
def check_extension(fileName):
    """
    Description:
    This function extracts the extension of the input file name
    and returns it as a string. Returning string does not contains
    the dot before the extension.

    Parameters:
    ----------
    fileName: str,
    the name of the file as a string

    Returns
    -------
    str,
    file extension
    """

    # we split the string with a dot separator
    sfn = fileName.split(sep='.')
    # now lets check if the splitting results in just two parts,
    # in which case, the second would be the file extension.
    if len(sfn) == 2:
        return sfn[-1]
    else:
        return sfn[-1]
        raise Warning('Warning: the input file name %s, may contain a strange extension. Beware to use the funcitonalities without taking care of it')

######################################################
def loadDFfromSAVfile(savFileName):
    """
    Description:
    This function takes an input SAV file name, loads it,
    and transform it into a usable and readable pandas
    dataframe.

    Parameters:
    ----------
    savFileName: str,
    complete path to the file name of the csv with the data.

    Returns:
    -------
    pandas.DataFrame,
    data frame with the sav data in a usable format.
    """

    # import necessary libraries
    import savReaderWriter as srw

    # read header
    with srw.SavReader(savFileName) as reader:
        header = reader.header

    raw_data = srw.SavReader(savFileName)
    raw_data_list = list(raw_data)
    df = pd.DataFrame(raw_data_list)

    df_new = pd.DataFrame()
    for num, col in zip(df.columns, header):
        if df[num].dtype == 'object':
            df_new[col.decode('UTF-8').upper()] = df[num].apply(lambda x: x.decode('UTF-8'))
        else:
            df_new[col.decode('UTF-8').upper()] = df[num]

    if 'PONDIH' not in df_new.columns:
        df_new['PONDIH'] = df_new['PONDERA']
    try:
        df_new.rename(columns={'PP04B_CAES': 'PP04B_COD', 'PP11B_CAES': 'PP11B_COD'}, inplace=True)
        return df_new
    except:
        return df_new

######################################################
### funciones y utilidades para realizar las estimaciones
def indRefUmap(x):
    """
    Description:
    This function takes a DataFrame (or a subset of DataFrame) of
    EPH (Encuesta permanente de Hogares) corresponding to individual
    data (not homes), and returns each persons factor conversion to
    reference unit of CB (Canasta Basica) for an adult male.
    
    Parameters:
    ----------
    x: pandas.DataFrame,
    DataFrame containing the data of EPH for individuals.

    returns:
    -------
    factor of conversion for reference unit of CB as to an adult male.
    """

    # # CH06 ==> edad (años cumplidos)
    # # CH05 ==> fecha de nacimiento (en gral. un datetime.datetime object)
    # # CH04 ==> sexo (1 varon, 2 mujer)
    # if x['CH06'] == -1:
    #     # edad -1 indica que es un bebe con 0 años cumplidos
    #     try:
    #         # intentamos estimar la edad en función de la fecha de nacimiento
    #         if x['CH05'].year == 2018:
    #             # si nacio en el 2018 tiene menos de medio año
    #             return refUnitMap[x['CH04']][0.5]
    #         else:
    #             # si nacion en el 2017, tiene mas de medio año
    #             return refUnitMap[x['CH04']][0.75]
    #     except:
    #         # aqui entra cuando es un bebé y no se puede estimar la edad a partir de su
    #         # fecha de nacimiento (no ocurre practicamente nunca). En ese caso asignamos
    #         # un valor medio por defecto.
    #         return (refUnitMap[x['CH04']][0.5]+refUnitMap[x['CH04']][0.75])/2
    if x['CH06'] == -1:
        # esto lo hacemos para compatibilizar con las eph anteriores a
        # 2016 (donde no existe el campo CH05 que es la fecha de nacimiento) 
        return refUnitMap[x['CH04']][0.75]
    else:
        # si no sucede nada de lo anterior, estamos en condiciones de llamar
        # el diccionario de MetaData con los factores de conversion.
        return refUnitMap[x['CH04']][x['CH06']]

###################################
### 
def setCanasta(x, fecha, tipo='a'):
    """
    Description:
    This function takes a DataFrame and applies a transformation
    to it to calculate the value of the Canasta Basica for each
    element of data in the DataFrame.

    Parameters:
    ----------
    x: pandas.DataFrame,
    the data frame from which to perform the CB calculation
    fecha: datetime.datetime,
    date for which the Canasta is being calculated
    tipo: str,
    select the type of Canasta Basica, it can be 'a' or 't'.
    {'a' for alimentaria and 't' for total}

    returns:
    -------
    returns the value of the canasta as a column for the DataFrame
    """

    # check if refU column is in DataFrame 
    refuCol = 'refU'
    assert refuCol in x
    if tipo == 'a':
        cba = CBA[fecha][x['REGION']]
        if np.isnan(cba):
            # si no hay registro para la region, usar el de GBA
            cba = CBA[fecha][1]
        return x[refuCol]*cba
    
    elif tipo == 't':
        cbt = CBT[fecha][x['REGION']]
        if np.isnan(cbt):
            # si no hay registro para la region, usar el de GBA
            cbt = CBT[fecha][1]
        return x[refuCol]*cbt

    else:
        raise Exception('something happened!')


#############################################################
### funcionalidades para los calculos de pobreza e indigencia
def indigencia_hogar(x_h, x_i, fecha, verbose=False):
    """
    Description:
    This function performs the calculation of the indigence in homes
    according to INDEC meassurments, taking as input DataFrames containing 
    EPH data from homes (x_h) and individuals (x_i). The 'fecha' input
    allows to use a line of indigence from a series of dates for which
    INDEC estimates this value.

    Parameters:
    ----------
    x_h: pandas.DataFrame,
    EPH data for homes.
    x_i: pandas.DataFrame,
    EPH data for individuals.
    fecha: datetime.datetime object,
    date for which to use the value of the line of indigence as meassured by INDEC.
    verbose: bool,
    variable para indicar si se quiere el resultado con salida a pantalla.

    Returns:
    -------
    float,
    percentage of the total homes under the line of indigence
    """

    # chequeamos que los DataFrames contengan la minima informacion necesaria
    # para realizar el calculo.
    xhcols = x_h.columns
    xicols = x_i.columns
    assert 'ITF'    in xhcols # x_h has ITF column (ingreso total familiar)
    assert 'CODUSU' in xhcols # x_h has CODUSU column (codigo de referencia de hogares)
    assert 'PONDIH' in xhcols # x_h has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
    assert 'IPCF'   in xhcols # x_h has IPCF column (ingreso per capita familiar del hogar)
    assert 'CODUSU' in xicols # x_i has CODUSU column (codigo de referencia de hogares)
    assert 'CH04'   in xicols # x_i has CH04 column (sexo)
    # assert 'CH05'   in xicols # x_i has CH05 column (fecha de nacimiento)
    assert 'CH06'   in xicols # x_i has CH06 column (edad, anios cumplidos)
    assert 'PONDIH' in xicols # x_i has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
    # chequeamos que fecha sea un de las fechas para las que tenemos
    # el valor de la canasta basica alimentaria de referencia de varon adulto
    assert fecha in CBA.keys() 

    # filtramos el DataFram y nos quedamos con uno nuevo que solo contenga
    # valores validos en la columna 'ITF'
    xh_clean = x_h[x_h['ITF'].notnull()]

    # armamos la columna 'refU' en el DataFrame de individuos
    refuCol = 'refU'
    if refuCol not in x_i.columns:
        x_i['refU'] = x_i.apply(indRefUmap, axis=1)

    # generamos un nuevo dataframe que agrega los refU por hogar
    x_ru = x_i.groupby(['CODUSU', 'NRO_HOGAR']).agg({'refU': 'sum'})

    # agregamos la columna con el refu por hogar al DataFrame de hogares y
    # lo guardamos en un nuevo DataFrame
    if refuCol not in x_h.columns:
        x_h  = xh_clean.merge(x_ru, how='inner', on=['CODUSU', 'NRO_HOGAR'])

    # calculamos los valores de canasta basica de referencia para cada hogar
    # queremos los valores para la fecha indicada:
    cbaCol = 'CBA_'+fecha.strftime('%Y%m%d')
    if cbaCol not in x_h.columns:
        x_h[cbaCol] = x_h.apply(setCanasta, args=(fecha, 'a'), axis=1)

    # Hogares por debajo de la linea de indigencia:
    data_cba = x_h[x_h['ITF']<x_h[cbaCol]]

    porc = 100*data_cba['PONDIH'].sum()/x_h['PONDIH'].sum()
    
    if verbose:
        print()
        print('Porcentaje de Hogares por debajo de la Linea de Indigencia:')
        print('%4.2f'%(porc))
        print()

    return porc

#########################################
### 
def pobreza_hogar(x_h, x_i, fecha, verbose=False):
    """
    Description:
    This function performs the calculation of the poverty in homes
    according to INDEC meassurments, taking as input DataFrames containing 
    EPH data from homes (x_h) and individuals (x_i). The 'fecha' input
    allows to use a line of poverty from a series of dates for which
    INDEC estimates this value.

    Parameters:
    ----------
    x_h: pandas.DataFrame,
    EPH data for homes.
    x_i: pandas.DataFrame,
    EPH data for individuals.
    fecha: datetime.datetime object,
    date for which to use the value of the line of poverty as meassured by INDEC.
    verbose: bool,
    variable to indicate if standard output is desired.

    Returns:
    -------
    float,
    percentage of the total homes under the line of poverty
    """

    # chequeamos que los DataFrames contengan la minima informacion necesaria
    # para realizar el calculo.
    xhcols = x_h.columns
    xicols = x_i.columns
    assert 'ITF'    in xhcols # x_h has ITF column (ingreso total familiar)
    assert 'CODUSU' in xhcols # x_h has CODUSU column (codigo de referencia de hogares)
    assert 'PONDIH' in xhcols # x_h has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
    assert 'IPCF'   in xhcols # x_h has IPCF column (ingreso per capita familiar del hogar)
    assert 'CODUSU' in xicols # x_i has CODUSU column (codigo de referencia de hogares)
    assert 'CH04'   in xicols # x_i has CH04 column (sexo)
    # assert 'CH05'   in xicols # x_i has CH05 column (fecha de nacimiento)
    assert 'CH06'   in xicols # x_i has CH06 column (edad, anios cumplidos)
    assert 'PONDIH' in xicols # x_i has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
    # chequeamos que fecha sea un de las fechas para las que tenemos
    # el valor de la canasta basica alimentaria de referencia de varon adulto
    assert fecha in CBT.keys() 

    # filtramos el DataFram y nos quedamos con uno nuevo que solo contenga
    # valores validos en la columna 'ITF'
    xh_clean = x_h[x_h['ITF'].notnull()]

    # armamos la columna 'refU' en el DataFrame de individuos
    refuCol = 'refU'
    if refuCol not in x_i.columns:
        x_i['refU'] = x_i.apply(indRefUmap, axis=1)

    # generamos un nuevo dataframe que agrega los refU por hogar
    x_ru = x_i.groupby(['CODUSU', 'NRO_HOGAR']).agg({'refU': 'sum'})

    # agregamos la columna con el refu por hogar al DataFrame de hogares y
    # lo guardamos en un nuevo DataFrame
    if refuCol not in x_h.columns:
        x_h  = xh_clean.merge(x_ru, how='inner', on=['CODUSU', 'NRO_HOGAR'])

    # calculamos los valores de canasta basica de referencia para cada hogar
    # queremos los valores para junio 2018:
    cbtCol = 'CBT_'+fecha.strftime('%Y%m%d')
    if cbtCol not in x_h.columns:
        x_h[cbtCol] = x_h.apply(setCanasta, args=(fecha, 't'), axis=1)
    
    # Hogares por debajo de la linea de indigencia:
    data_cbt = x_h[x_h['ITF']<x_h[cbtCol]]

    porc = 100*data_cbt['PONDIH'].sum()/x_h['PONDIH'].sum()
    
    if verbose:
        print()
        print('Porcentaje de Hogares por debajo de la Linea de Pobreza:')
        print('%4.2f'%(porc))
        print()

    return porc

##########################################
###
def pobYind_hogar(x_h, x_i, fecha, ind='todos', verbose=False):
    """
    Description:
    This function performs the calculation of the poverty and/or indigence 
    in homes according to INDEC meassurments, taking as input DataFrames
    containing EPH data from homes (x_h) and individuals (x_i). The 'fecha'
    input allows to use a line of poverty from a series of dates for which
    INDEC estimates these values.

    Parameters:
    ----------
    x_h: pandas.DataFrame,
    EPH data for homes.
    x_i: pandas.DataFrame,
    EPH data for individuals.
    fecha: datetime.datetime object,
    date for which to use the value of the line of poverty as meassured by INDEC.
    ind: str,
    with this variable we indicate if we want indigence ('ind'), poverty ('pob') or both.
    verbose: bool,
    variable to indicate if standard output is desired.

    Returns:
    -------
    dict,
    dictionary containing the values of the indices
    """

    # chequeamos que los DataFrames contengan la minima informacion necesaria
    # para realizar el calculo.
    xhcols = x_h.columns
    xicols = x_i.columns
    assert 'ITF'    in xhcols # x_h has ITF column (ingreso total familiar)
    assert 'CODUSU' in xhcols # x_h has CODUSU column (codigo de referencia de hogares)
    assert 'PONDIH' in xhcols # x_h has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
    assert 'IPCF'   in xhcols # x_h has IPCF column (ingreso per capita familiar del hogar)
    assert 'CODUSU' in xicols # x_i has CODUSU column (codigo de referencia de hogares)
    assert 'CH04'   in xicols # x_i has CH04 column (sexo)
    # assert 'CH05'   in xicols # x_i has CH05 column (fecha de nacimiento)
    assert 'CH06'   in xicols # x_i has CH06 column (edad, anios cumplidos)
    assert 'PONDIH' in xicols # x_i has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
    # chequeamos que fecha sea un de las fechas para las que tenemos
    # el valor de la canasta basica alimentaria de referencia de varon adulto
    assert fecha in CBT.keys() 

    outDict = {}
    if ind[:3] == 'ind':
        porc_ind = indigencia_hogar(x_h, x_i, fecha, verbose=verbose)
        outDict['ind'] = porc_ind
        return outDict
        
    elif ind[:3] == 'pob':
        porc_pob = pobreza_hogar(x_h, x_i, fecha, verbose=verbose)
        outDict['pob'] = porc_pob
        return outDict
    
    else:
        # filtramos el DataFrame y nos quedamos con uno nuevo que solo contenga
        # valores validos en la columna 'ITF'
        xh_clean = x_h[x_h['ITF'].notnull()]

        # armamos la columna 'refU' en el DataFrame de individuos
        x_i['refU'] = x_i.apply(indRefUmap, axis=1)

        # generamos un nuevo dataframe que agrega los refU por hogar
        x_ru = x_i.groupby(['CODUSU', 'NRO_HOGAR']).agg({'refU': 'sum'})

        # agregamos la columna con el refu por hogar al DataFrame de hogares y
        # lo guardamos en un nuevo DataFrame
        x_ru  = xh_clean.merge(x_ru, how='inner', on=['CODUSU', 'NRO_HOGAR'])

        # calculamos los valores de canasta basica de referencia para cada hogar
        # queremos los valores para junio 2018:
        x_ru['CBA'] = x_ru.apply(setCanasta, args=(fecha, 'a'), axis=1)
        x_ru['CBT'] = x_ru.apply(setCanasta, args=(fecha, 't'), axis=1)        
        # x_ru['CBT'] = x_ru['refU']*CBT[fecha][x_ru['REGION']]
        # x_ru['CBA'] = x_ru['refU']*CBA[fecha][x_ru['REGION']]
        
        # Hogares por debajo de la linea de indigencia:        
        data_cbt = x_ru[x_ru['ITF']<x_ru['CBT']]
        data_cba = x_ru[x_ru['ITF']<x_ru['CBA']]
        
        porc_pob = 100*data_cbt['PONDIH'].sum()/x_ru['PONDIH'].sum()
        porc_ind = 100*data_cba['PONDIH'].sum()/x_ru['PONDIH'].sum()
        
        if verbose:
            print()
            print('Porcentaje de Hogares por debajo de la Linea de Pobreza:')
            print('%4.2f'%(porc_pob))
            print()
            print('Porcentaje de Hogares por debajo de la Linea de Indigencia:')
            print('%4.2f'%(porc_ind))

        outDict['ind'] = porc_ind
        outDict['pob'] = porc_pob
        
        return outDict
    
########################################
###
def pobreza_personas(x_h, x_i, fecha, verbose=False):
    """
    Description:
    This function performs the calculation of the poverty of persons
    according to INDEC meassurments, taking as input DataFrames containing 
    EPH data from homes (x_h) and individuals (x_i). The 'fecha' input
    allows to use a line of poverty from a series of dates for which
    INDEC estimates this value.

    Parameters:
    ----------
    x_h: pandas.DataFrame,
    EPH data for homes.
    x_i: pandas.DataFrame,
    EPH data for individuals.
    fecha: datetime.datetime object,
    date for which to use the value of the line of poverty as meassured by INDEC.
    verbose: bool,
    variable to indicate if standard output is desired.

    Returns:
    -------
    float,
    percentage of the total persons under the line of poverty
    """

    # chequeamos que los DataFrames contengan la minima informacion necesaria
    # para realizar el calculo.
    xhcols = x_h.columns
    xicols = x_i.columns
    assert 'ITF'    in xhcols # x_h has ITF column (ingreso total familiar)
    assert 'CODUSU' in xhcols # x_h has CODUSU column (codigo de referencia de hogares)
    assert 'PONDIH' in xhcols # x_h has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
    assert 'IPCF'   in xhcols # x_h has IPCF column (ingreso per capita familiar del hogar)
    assert 'CODUSU' in xicols # x_i has CODUSU column (codigo de referencia de hogares)
    assert 'CH04'   in xicols # x_i has CH04 column (sexo)
    # assert 'CH05'   in xicols # x_i has CH05 column (fecha de nacimiento)
    assert 'CH06'   in xicols # x_i has CH06 column (edad, anios cumplidos)
    assert 'PONDIH' in xicols # x_i has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
    # chequeamos que fecha sea un de las fechas para las que tenemos
    # el valor de la canasta basica alimentaria de referencia de varon adulto
    assert fecha in CBT.keys()

    # filtramos el DataFram y nos quedamos con uno nuevo que solo contenga
    # valores validos en la columna 'ITF'
    xh_clean = x_h[x_h['ITF'].notnull()]

    # armamos la columna 'refU' en el DataFrame de individuos
    refuCol = 'refU'
    if refuCol not in x_i.columns:
        x_i['refU'] = x_i.apply(indRefUmap, axis=1)

    # generamos un nuevo DataFrame con la informacion en x_i y el IPCF.
    ipcfCol = 'IPCF'
    if ipcfCol not in x_i.columns:
        x_i = x_i.merge(xh_clean[['CODUSU', 'NRO_HOGAR', 'IPCF']], on=['CODUSU', 'NRO_HOGAR'], how='inner')
        x_i.rename(columns={'IPCF_y': 'IPCF'}, inplace=True)
    
    # calculamos los valores de canasta basica de referencia para cada hogar
    # queremos los valores para junio 2018:
    cbtCol = 'CBT_'+fecha.strftime('%Y%m%d')
    if cbtCol not in x_i.columns:
        x_i[cbtCol] = x_i.apply(setCanasta, args=(fecha, 't'), axis=1)

    # personas por debajo de la linea de pobreza:
    data_cbt = x_i[ x_i[ipcfCol] < x_i[cbtCol] ]

    porc = 100*data_cbt['PONDIH'].sum()/x_i['PONDIH'].sum()
    
    if verbose:
        print()
        print('Porcentaje de Personas por debajo de la Linea de Pobreza:')
        print('%4.2f'%(porc))
        print()

    return porc

#####################################################
###
def indigencia_personas(x_h, x_i, fecha, verbose=False):
    """
    Description:
    This function performs the calculation of the indigence of persons
    according to INDEC meassurments, taking as input DataFrames containing 
    EPH data from homes (x_h) and individuals (x_i). The 'fecha' input
    allows to use a line of indigence from a series of dates for which
    INDEC estimates this value.

    Parameters:
    ----------
    x_h: pandas.DataFrame,
    EPH data for homes.
    x_i: pandas.DataFrame,
    EPH data for individuals.
    fecha: datetime.datetime object,
    date for which to use the value of the line of indigence as meassured by INDEC.
    verbose: bool,
    variable para indicar si se quiere el resultado con salida a pantalla.

    Returns:
    -------
    float,
    percentage of the total persons under the line of indigence
    """

    # chequeamos que los DataFrames contengan la minima informacion necesaria
    # para realizar el calculo.
    xhcols = x_h.columns
    xicols = x_i.columns
    assert 'ITF'    in xhcols # x_h has ITF column (ingreso total familiar)
    assert 'CODUSU' in xhcols # x_h has CODUSU column (codigo de referencia de hogares)
    assert 'PONDIH' in xhcols # x_h has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
    assert 'IPCF'   in xhcols # x_h has IPCF column (ingreso per capita familiar del hogar)
    assert 'CODUSU' in xicols # x_i has CODUSU column (codigo de referencia de hogares)
    assert 'CH04'   in xicols # x_i has CH04 column (sexo)
    # assert 'CH05'   in xicols # x_i has CH05 column (fecha de nacimiento)
    assert 'CH06'   in xicols # x_i has CH06 column (edad, anios cumplidos)
    assert 'PONDIH' in xicols # x_i has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
    # chequeamos que fecha sea un de las fechas para las que tenemos
    # el valor de la canasta basica alimentaria de referencia de varon adulto
    assert fecha in CBA.keys() 

    # filtramos el DataFram y nos quedamos con uno nuevo que solo contenga
    # valores validos en la columna 'ITF'
    xh_clean = x_h[x_h['ITF'].notnull()]

    # armamos la columna 'refU' en el DataFrame de individuos
    refuCol = 'refU'
    if refuCol not in x_i.columns:
        x_i['refU'] = x_i.apply(indRefUmap, axis=1)

    # generamos un nuevo DataFrame con la informacion en x_i y el IPCF.
    ipcfCol = 'IPCF'
    if ipcfCol not in x_i.columns:
        x_i = x_i.merge(xh_clean[['CODUSU', 'NRO_HOGAR', 'IPCF']], on=['CODUSU', 'NRO_HOGAR'], how='inner')
        x_i.rename(columns={ipcfCol+'_y': ipcfCol}, inplace=True)

    # calculamos los valores de canasta basica de referencia para cada hogar
    # queremos los valores para junio 2018:
    cbaCol = 'CBA_'+fecha.strftime('%Y%m%d')
    if cbaCol not in x_i.columns:
        x_i[cbaCol] = x_i.apply(setCanasta, args=(fecha, 'a'), axis=1)
        
    # Hogares por debajo de la linea de indigencia:
    data_cba = x_i[ x_i[ipcfCol] < x_i[cbaCol] ]


    porc = 100*data_cba['PONDIH'].sum()/x_i['PONDIH'].sum()
    
    if verbose:
        print()
        print('Porcentaje de Personas por debajo de la Linea de Indigencia:')
        print('%4.2f'%(porc))
        print()

    return porc

##############################################
###
def pobYind_personas(x_h, x_i, fecha, ind='todos', verbose=False):
    """
    Description:
    This function performs the calculation of the poverty and/or indigence 
    in persons according to INDEC meassurments, taking as input DataFrames
    containing EPH data from homes (x_h) and individuals (x_i). The 'fecha'
    input allows to use a line of poverty from a series of dates for which
    INDEC estimates these values.

    Parameters:
    ----------
    x_h: pandas.DataFrame,
    EPH data for homes.
    x_i: pandas.DataFrame,
    EPH data for individuals.
    fecha: datetime.datetime object,
    date for which to use the value of the line of poverty as meassured by INDEC.
    ind: str,
    with this variable we indicate if we want indigence ('ind'), poverty ('pob') or both.
    verbose: bool,
    variable to indicate if standard output is desired.

    Returns:
    -------
    dict,
    dictionary containing the values of the indices
    """

    # chequeamos que los DataFrames contengan la minima informacion necesaria
    # para realizar el calculo.
    xhcols = x_h.columns
    xicols = x_i.columns
    assert 'ITF'    in xhcols # x_h has ITF column (ingreso total familiar)
    assert 'CODUSU' in xhcols # x_h has CODUSU column (codigo de referencia de hogares)
    assert 'PONDIH' in xhcols # x_h has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
    assert 'IPCF'   in xhcols # x_h has IPCF column (ingreso per capita familiar del hogar)
    assert 'CODUSU' in xicols # x_i has CODUSU column (codigo de referencia de hogares)
    assert 'CH04'   in xicols # x_i has CH04 column (sexo)
    # assert 'CH05'   in xicols # x_i has CH05 column (fecha de nacimiento)
    assert 'CH06'   in xicols # x_i has CH06 column (edad, anios cumplidos)
    assert 'PONDIH' in xicols # x_i has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
    # chequeamos que fecha sea un de las fechas para las que tenemos
    # el valor de la canasta basica alimentaria de referencia de varon adulto
    assert fecha in CBT.keys() 

    outDict = {}
    if ind[:3] == 'ind':
        porc_ind = indigencia_personas(x_h, x_i, fecha, verbose=verbose)
        outDict['ind'] = porc_ind
        return outDict
        
    elif ind[:3] == 'pob':
        porc_pob = pobreza_personas(x_h, x_i, fecha, verbose=verbose)
        outDict['pob'] = porc_pob
        return outDict
    
    else:
        # filtramos el DataFrame y nos quedamos con uno nuevo que solo contenga
        # valores validos en la columna 'ITF'
        xh_clean = x_h[x_h['ITF'].notnull()]

        # armamos la columna 'refU' en el DataFrame de individuos
        x_i['refU'] = x_i.apply(indRefUmap, axis=1)

        # generamos un nuevo DataFrame con la informacion en x_i y el IPCF.
        x_ipcf = x_i.merge(xh_clean[['CODUSU', 'NRO_HOGAR', 'IPCF']], on=['CODUSU', 'NRO_HOGAR'], how='inner')
        x_ipcf.rename(columns={'IPCF_y': 'IPCF'}, inplace=True)
        
        # calculamos los valores de canasta basica de referencia para cada hogar
        x_ipcf['CBT'] = x_ipcf.apply(setCanasta, args=(fecha, 't'), axis=1)
        x_ipcf['CBA'] = x_ipcf.apply(setCanasta, args=(fecha, 'a'), axis=1)
        
        # Personas por debajo de la linea de indigencia:
        data_cbt = x_ipcf[x_ipcf['IPCF']<x_ipcf['CBT']]
        data_cba = x_ipcf[x_ipcf['IPCF']<x_ipcf['CBA']]
        
        porc_pob = 100*data_cbt['PONDIH'].sum()/x_ipcf['PONDIH'].sum()
        porc_ind = 100*data_cba['PONDIH'].sum()/x_ipcf['PONDIH'].sum()
        
        if verbose:
            print()
            print('Porcentaje de Personas por debajo de la Linea de Pobreza:')
            print('%4.2f'%(porc_pob))
            print()
            print('Porcentaje de Personas por debajo de la Linea de Indigencia:')
            print('%4.2f'%(porc_ind))

        outDict['ind'] = porc_ind
        outDict['pob'] = porc_pob
        
        return outDict

#############################################
###
def pobreza_indigencia_regiones(x_h, x_i, fecha, verbose=False):
    """
    Description:
    This function calculates the poverty and indigence values
    of populations segregated by regions
    """
    # Por regiones`
    # importlib.reload(indecT)
    
    data_dict = {}
    for data_h, data_i in zip(x_h.groupby('REGION'), x_i.groupby('REGION')):
        reg = region_map[data_h[0]]
        dataH = deepcopy(data_h[1])
        dataI = deepcopy(data_i[1])
        data_dict[reg] = {}
        data_dict[reg]['indigencia_personas'] = indigencia_personas(dataH, dataI, fecha, verbose=False)
        data_dict[reg]['indigencia_hogares'] = indigencia_hogar(dataH, dataI, fecha, verbose=False)
        data_dict[reg]['pobreza_personas'] = pobreza_personas(dataH, dataI, fecha, verbose=False)
        data_dict[reg]['pobreza_hogares'] = pobreza_hogar(dataH, dataI, fecha, verbose=False)

    return pd.DataFrame(data_dict).transpose()

#################################
###
class IndecClass(object):
    """
    Description:
    this is a base class for storing datasets that can be reused and modified
    aggregating columns and other kind of information.
    """

    ##########################################
    ###
    def __init__(self, EPHinput_hogares=None, EPHinput_personas=None, inputFilePath='./'):
        """
        Description:
        Initial constructor of the IndecClass base class
        
        Parameters:
        ----------
        inputFilePath: str,
        absolute path where the EPH files are located in case files are given
        EPHinput_hogares: str or pd.DataFrame,
        file name of the EPH or pd.DataFrame with EPH info of raw data
        for hogares
        EPHinput_personas: str or pd.DataFrame,
        file name of the EPH or pd.DataFrame with EPH info of raw data
        for individuos

        Returns:
        -------
        None
        """

        self.EPHpath = inputFilePath
        
        # load the data into corresponding DataFrames
        # in the case fileNames where provided
        # hogares DataFrame
        if EPHinput_hogares is not None:
            if isinstance(EPHinput_hogares, str):
                self.EPHhogaresFile  = EPHinput_hogares
                self.set_hogaresDF()
            elif isinstance(EPHinput_hogares, pd.DataFrame):
                self.set_hogaresDF(inputDF=EPHinput_hogares)
            else:
                raise Exception('wrong data input for EPHinput_hogares...')
        # personas DataFrame
        if EPHinput_personas is not None:
            if isinstance(EPHinput_personas, str):
                self.EPHpersonasFile = EPHinput_personas
                self.set_personasDF()
            elif isinstance(EPHinput_personas, pd.DataFrame):
                self.set_personasDF(inputDF=EPHinput_personas)
            else:
                raise Exception('wrong data input for EPHinput_personas...')

        # set the results attribute as an empty dict first
        self.result = {}
        
        return None
                  
    ############################################
    ### setter for input file names
    def set_fileNameHogares(self, inputFileName):
        """
        Description:
        this function is used to set a new filename to the 
        self.EPHhogaresFile variable.

        Parameters:
        ----------
        inputFileName: str,
        string with the name of the file to use as input for the Hogares
        EPH data.
        
        Returns:
        -------
        Nonde
        """
        
        self.EPHhogaresFile = inputFileName

        return None

    ############################################
    ### setter for input file names
    def set_fileNamePersonas(self, inputFileName):
        """
        Description:
        this function is used to set a new filename to the 
        self.EPHpersonasFile variable.

        Parameters:
        ----------
        inputFileName: str,
        string with the name of the file to use as input for the Individual persons
        EPH data.
        
        Returns:
        -------
        None
        """
        
        self.EPHpersonasFile = inputFileName

        return None

    ###########################################3
    ###
    def set_personasDF(self, inputFileName=None, inputDF=None):
        """
        Description:
        This function uses either the file saved in the self.EPHpersonasFile 
        attribute or the one given as inputFileName to set the personas DataFrame
        used to perform the calculation of the indexes
        
        Parameters:
        ----------
        inputFileName: str (optional),
        optionally, a file name can be given which will be set as the EPHpersonasFile 
        attribute of the class instance and the DataFrame will be loaded with it's data.
        inputDF: pandas.DataFrame (optional),
        optionally, a DataFrame can be passed in order for it to be set as
        the self.personasDF. If this is the case, make sure that inputFileName is None.

        Returns:
        -------
        None
        """
        
        # first check if inputFileName was given
        if inputFileName is not None:
            # if so, then set it as the input file for the class instance
            self.set_fileNamePersonas(inputFileName)
        elif inputDF is not None:
            # if a DataFrame is given as input, then set it directly and return
            self.personasDF = inputDF
            print('DataFrame set successfully...')

            return None
        else:
            # it was not given we need to make sure that attribute self.EPHpersonasFile exists
            # assert self.EPHpersonasFile
            assert self.EPHpersonasFile, 'Attribute EPHpersonasFile is not set. Try to set it with the set_fileNamePersonas method or by providing a fileName to the set_personasDF method.'

        # Load data into DataFrame
        print('Loading Individual persons data into DataFrame for processing...')
        print('this can take a while...')
        
        # check file extension
        ext = check_extension(self.EPHpersonasFile)
        if ext == 'xls':
            self.personasDF = pd.read_excel(self.EPHpath + self.EPHpersonasFile)
        elif ext == 'csv':
            self.personasDF = pd.read_csv(self.EPHpath + self.EPHpersonasFile)
        else:
            raise Exception('input file extension %s not supported for loadeing. Try xls or csv files.'%self.ext)
        print('...finished loading Individual persons data. Data loaded successfully')

        return None

    ##########################################
    ###
    def set_hogaresDF(self, inputFileName=None, inputDF=None):
        """
        Description:
        This function uses either the file saved in the self.EPHhogaresFile
        attribute or the one given as inputFileName to set the hogares DataFrame
        used to perform the calculation of the indexes
        
        Parameters:
        ----------
        inputFileName: str (optional),
        optionally, a file name can be given which will be set as the EPHhogaresFile 
        attribute of the class instance and the DataFrame will be loaded with it's data.
        inputDF: pandas.DataFrame (optional),
        optionally, a DataFrame can be passed in order for it to be set as
        the self.personasDF. If this is the case, make sure that inputFileName is None.

        Returns:
        -------
        None
        """
        
        # first check if inputFileName was given
        if inputFileName is not None:
            # if so, then set it as the input file for the class instance
            self.set_fileNameHogares(inputFileName)
            
        elif inputDF is not None:
            # if a DataFrame is given as input, then set it directly and return
            self.hogaresDF = inputDF
            print('DataFrame set successfully...')            
            return None
        
        else:
            # it was not given we need to make sure that attribute self.EPHpersonasFile exists
            assert self.EPHhogaresFile, 'Attribute EPHhogaresFile is not set. Try to set it with the set_fileNameHogares method or by providing a fileName to the set_hogaresDF method.'

        print('Loading Homes data into DataFrame for processing...')
        print('this can take a while...')
        # check file extension
        ext = check_extension(self.EPHhogaresFile)
        if ext == 'xls':
            self.hogaresDF  = pd.read_excel(self.EPHpath + self.EPHhogaresFile)
        elif ext == 'csv':
            self.hogaresDF  = pd.read_csv(self.EPHpath + self.EPHhogaresFile)
        else:
            raise Exception('input file extension %s not supported for loadeing. Try xls or csv files.'%self.ext)
        print('...finished loading Homes data. Data loaded successfully')

        return None
    
    ###################################################
    ### setter methods
    def setRefU(self, on=None):
        """
        Description:
        This method sets the refU column on either one of the base DFs of 
        the class, or on both (hogaresDF and personasDF).
        
        Parameters:
        ----------
        on: str (default None, which means on both dfs),
        string indicating on which base df to set the refU column
        {'per', 'hog', 'Per', 'Hog', 'PER', 'HOG', 'Personas,
        'Hogares', 'PERSON', HoGaRE'}.
        
        Returns:
        -------
        None
        """

        # if a specific dataframe is passed
        if on:
            dfName = on.lower()[:2]
            # only on personasDF
            if dfName == 'per':
                self.setRefuPersonas()
                
            # only on hogaresDF
            elif dfName == 'hog':
                # hogaresDF refu needs to have personasDF refu set
                # so it actually works for both dfs
                self.setRefuHogares()
        else:
            # if both then do both
            # we could be dealing with both just with the call
            # to the setRefuHogares method, but for consistency we call both
            self.setRefuPersonas()
            self.setRefuHogares()

    ########################
    ### setter for the reference unit in the hogares
    def setRefuHogares(self):
        """
        Description:
        This method sets the refU column on the hogaresDF base DataFrame
        of the class.
        
        Returns:
        -------
        None
        """
        # Only perform addition of refU column if it's not already there
        refuCol = 'refU'
        if refuCol not in self.hogaresDF.columns:
            # to set refU on hogaresDF, we need refU on personasDF first
            self.setRefuPersonas()
            
            # generamos un dataframe que agrega los refU por hogar
            x_ru = self.personasDF.groupby(['CODUSU', 'NRO_HOGAR']).agg({'refU': 'sum'})
    
            # agregamos la columna con el refu por hogar al DataFrame de hogares y
            # lo guardamos en un nuevo DataFrame
            self.hogaresDF = self.hogaresDF.merge(x_ru, how='inner', on=['CODUSU', 'NRO_HOGAR'])

        else:
            # if it's already part of the dataframe, do nothing
            pass
        
        return None

    #################################################
    ###
    def setRefuPersonas(self):
        """
        Description:
        This method sets the refU column on the personasDF base DataFrame
        of the class.
        
        Returns:
        -------
        None
        """
        
        # Only perform addition of refU column if it's not already there
        refuCol = 'refU'
        if refuCol not in self.personasDF.columns:
            self.personasDF[refuCol] = self.personasDF.apply(indRefUmap, axis=1)

        else:
            # if it's already part of the dataframe, do nothing
            pass
        
        return None

    ###################################################
    ### calculation methods
    def indigencia_personas(self, fecha, verbose=False):
        """
        Description:
        This method performs the calculation of the indigence of persons
        according to INDEC meassurments, using the EPH datasets that need
        to be loaded into the personasDF and hogaresDF attributes. 
        The 'fecha' input allows to use a line of indigence from a series of
        dates for which the INDEC estimates this value.

        Parameters:
        ----------
        fecha: datetime.datetime object,
        date for which to use the value of the line of indigence as meassured by INDEC.
        verbose: bool,
        variable para indicar si se quiere el resultado con salida a pantalla.

        Returns:
        -------
        float,
        percentage of the total persons under the line of indigence
        """

        # chequeamos que los DataFrames contengan la minima informacion necesaria
        # para realizar el calculo.
        xhcols = self.hogaresDF.columns
        xicols = self.personasDF.columns
        assert 'ITF'    in xhcols # x_h has ITF column (ingreso total familiar)
        assert 'CODUSU' in xhcols # x_h has CODUSU column (codigo de referencia de hogares)
        assert 'PONDIH' in xhcols # x_h has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
        assert 'IPCF'   in xhcols # x_h has IPCF column (ingreso per capita familiar del hogar)
        assert 'CODUSU' in xicols # x_i has CODUSU column (codigo de referencia de hogares)
        assert 'CH04'   in xicols # x_i has CH04 column (sexo)
        # assert 'CH05'   in xicols # x_i has CH05 column (fecha de nacimiento)
        assert 'CH06'   in xicols # x_i has CH06 column (edad, anios cumplidos)
        assert 'PONDIH' in xicols # x_i has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
        # chequeamos que fecha sea un de las fechas para las que tenemos
        # el valor de la canasta basica alimentaria de referencia de varon adulto
        assert fecha in CBA.keys() 

        # filtramos el DataFram y nos quedamos con uno nuevo que solo contenga
        # valores validos en la columna 'ITF'
        xh_clean = self.hogaresDF[self.hogaresDF['ITF'].notnull()]
        
        # armamos la columna 'refU_fecha' en el DataFrame de individuos
        # primero chequeamos que no exista ya.
        refuCol = 'refU'
        if refuCol not in self.personasDF.columns:
            # si no existe lo creamos
            self.personasDF[refuCol] = self.personasDF.apply(indRefUmap, axis=1)

        # generamos un nuevo DataFrame con la informacion en x_i y el IPCF.
        ipcfCol = 'IPCF'
        if ipcfCol not in self.personasDF.columns:
            self.personasDF = self.personasDF.merge(xh_clean[['CODUSU', 'NRO_HOGAR', 'IPCF']], on=['CODUSU', 'NRO_HOGAR'], how='inner')
            self.personasDF.rename(columns={'IPCF_y': 'IPCF'}, inplace=True)

        # calculamos los valores de canasta basica de referencia para cada hogar
        # queremos los valores para junio 2018:
        cbaCol = 'CBA_'+fecha.strftime('%Y%m%d')
        if cbaCol not in self.personasDF.columns:
            self.personasDF[cbaCol] = self.personasDF.apply(setCanasta, args=(fecha, 'a'), axis=1)
            
        # Hogares por debajo de la linea de indigencia:
        data_cba = self.personasDF[self.personasDF[ipcfCol]<self.personasDF[cbaCol]]

        porc = 100*data_cba['PONDIH'].sum()/self.personasDF['PONDIH'].sum()
    
        if verbose:
            print()
            print('Porcentaje de Personas por debajo de la Linea de Indigencia:')
            print('%4.2f'%(porc))
            print()

        return porc


    def pobreza_personas(self, fecha, verbose=False):
        """
        Description:
        This function performs the calculation of the poverty of persons
        according to INDEC meassurments, taking as input DataFrames containing 
        EPH data from homes (x_h) and individuals (x_i). The 'fecha' input
        allows to use a line of poverty from a series of dates for which
        INDEC estimates this value.
    
        Parameters:
        ----------
        x_h: pandas.DataFrame,
        EPH data for homes.
        x_i: pandas.DataFrame,
        EPH data for individuals.
        fecha: datetime.datetime object,
        date for which to use the value of the line of poverty as meassured by INDEC.
        verbose: bool,
        variable to indicate if standard output is desired.
    
        Returns:
        -------
        float,
        percentage of the total persons under the line of poverty
        """
    
        # chequeamos que los DataFrames contengan la minima informacion necesaria
        # para realizar el calculo.
        xhcols = self.hogaresDF.columns
        xicols = self.personasDF.columns
        assert 'ITF'    in xhcols # x_h has ITF column (ingreso total familiar)
        assert 'CODUSU' in xhcols # x_h has CODUSU column (codigo de referencia de hogares)
        assert 'PONDIH' in xhcols # x_h has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
        assert 'IPCF'   in xhcols # x_h has IPCF column (ingreso per capita familiar del hogar)
        assert 'CODUSU' in xicols # x_i has CODUSU column (codigo de referencia de hogares)
        assert 'CH04'   in xicols # x_i has CH04 column (sexo)
        # assert 'CH05'   in xicols # x_i has CH05 column (fecha de nacimiento)
        assert 'CH06'   in xicols # x_i has CH06 column (edad, anios cumplidos)
        assert 'PONDIH' in xicols # x_i has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
        # chequeamos que fecha sea un de las fechas para las que tenemos
        # el valor de la canasta basica alimentaria de referencia de varon adulto
        assert fecha in CBT.keys()
    
        # filtramos el DataFram y nos quedamos con uno nuevo que solo contenga
        # valores validos en la columna 'ITF'
        xh_clean = self.hogaresDF[self.hogaresDF['ITF'].notnull()]
    
        # armamos la columna 'refU' en el DataFrame de individuos
        refuCol = 'refU'
        if refuCol not in self.personasDF.columns:
            # si no existe lo creamos
            self.personasDF[refuCol] = self.personasDF.apply(indRefUmap, axis=1)

        
        # generamos un nuevo DataFrame con la informacion en x_i y el IPCF.
        ipcfCol = 'IPCF'
        if ipcfCol not in self.personasDF.columns:
            self.personasDF = self.personasDF.merge(xh_clean[['CODUSU', 'NRO_HOGAR', 'IPCF']], on=['CODUSU', 'NRO_HOGAR'], how='inner')
            self.personasDF.rename(columns={'IPCF_y': 'IPCF'}, inplace=True)
        
        # calculamos los valores de canasta basica de referencia para cada hogar
        # queremos los valores para junio 2018:
        cbtCol = 'CBT_'+fecha.strftime('%Y%m%d')
        if cbtCol not in self.personasDF.columns:
            self.personasDF[cbtCol] = self.personasDF.apply(setCanasta, args=(fecha, 't'), axis=1)

        # personas por debajo de la linea de pobreza:
        data_cbt = self.personasDF[self.personasDF[ipcfCol]<self.personasDF[cbtCol]]
    
        porc = 100*data_cbt['PONDIH'].sum()/self.personasDF['PONDIH'].sum()
        
        if verbose:
            print()
            print('Porcentaje de Personas por debajo de la Linea de Pobreza:')
            print('%4.2f'%(porc))
            print()
    
        return porc

    def pobreza_hogar(self, fecha, verbose=False):
        """
        Description:
        This function performs the calculation of the poverty in homes
        according to INDEC meassurments, taking as input DataFrames containing 
        EPH data from homes (x_h) and individuals (x_i). The 'fecha' input
        allows to use a line of poverty from a series of dates for which
        INDEC estimates this value.
    
        Parameters:
        ----------
        fecha: datetime.datetime object,
        date for which to use the value of the line of poverty as meassured by INDEC.
        verbose: bool,
        variable to indicate if standard output is desired.
    
        Returns:
        -------
        float,
        percentage of the total homes under the line of poverty
        """
    
        # chequeamos que los DataFrames contengan la minima informacion necesaria
        # para realizar el calculo.
        xhcols = self.hogaresDF.columns
        xicols = self.personasDF.columns
        assert 'ITF'    in xhcols # x_h has ITF column (ingreso total familiar)
        assert 'CODUSU' in xhcols # x_h has CODUSU column (codigo de referencia de hogares)
        assert 'PONDIH' in xhcols # x_h has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
        assert 'IPCF'   in xhcols # x_h has IPCF column (ingreso per capita familiar del hogar)
        assert 'CODUSU' in xicols # x_i has CODUSU column (codigo de referencia de hogares)
        assert 'CH04'   in xicols # x_i has CH04 column (sexo)
        # assert 'CH05'   in xicols # x_i has CH05 column (fecha de nacimiento)
        assert 'CH06'   in xicols # x_i has CH06 column (edad, anios cumplidos)
        assert 'PONDIH' in xicols # x_i has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
        # chequeamos que fecha sea un de las fechas para las que tenemos
        # el valor de la canasta basica alimentaria de referencia de varon adulto
        assert fecha in CBT.keys() 
    
        # filtramos el DataFram y nos quedamos con uno nuevo que solo contenga
        # valores validos en la columna 'ITF'
        xh_clean = self.hogaresDF[self.hogaresDF['ITF'].notnull()]
    
        # armamos la columna 'refU' en el DataFrame de individuos
        refuCol = 'refU'
        if refuCol not in self.personasDF.columns:
            # si no existe lo creamos
            self.personasDF[refuCol] = self.personasDF.apply(indRefUmap, axis=1)
    
        # generamos un nuevo dataframe que agrega los refU por hogar
        x_ru = self.personasDF.groupby(['CODUSU', 'NRO_HOGAR']).agg({'refU': 'sum'})
    
        # agregamos la columna con el refu por hogar al DataFrame de hogares y
        # lo guardamos en un nuevo DataFrame
        if refuCol not in self.hogaresDF.columns:
            self.hogaresDF = self.hogaresDF.merge(x_ru, how='inner', on=['CODUSU', 'NRO_HOGAR'])
    
        # calculamos los valores de canasta basica de referencia para cada hogar
        # queremos los valores para fecha:
        cbtCol = 'CBT_'+fecha.strftime('%Y%m%d')
        if cbtCol not in self.hogaresDF.columns:
            self.hogaresDF[cbtCol] = self.hogaresDF.apply(setCanasta, args=(fecha, 't'), axis=1)
    
        # Hogares por debajo de la linea de indigencia:
        data_cbt = self.hogaresDF[self.hogaresDF['ITF']<self.hogaresDF[cbtCol]]
    
        porc = 100*data_cbt['PONDIH'].sum()/self.hogaresDF['PONDIH'].sum()
        
        if verbose:
            print()
            print('Porcentaje de Hogares por debajo de la Linea de Pobreza:')
            print('%4.2f'%(porc))
            print()
    
        return porc

    def indigencia_hogar(self, fecha, verbose=False):
        """
        Description:
        This function performs the calculation of the poverty in homes
        according to INDEC meassurments, taking as input DataFrames containing 
        EPH data from homes (x_h) and individuals (x_i). The 'fecha' input
        allows to use a line of poverty from a series of dates for which
        INDEC estimates this value.
    
        Parameters:
        ----------
        fecha: datetime.datetime object,
        date for which to use the value of the line of poverty as meassured by INDEC.
        verbose: bool,
        variable to indicate if standard output is desired.
    
        Returns:
        -------
        float,
        percentage of the total homes under the line of poverty
        """
    
        # chequeamos que los DataFrames contengan la minima informacion necesaria
        # para realizar el calculo.
        xhcols = self.hogaresDF.columns
        xicols = self.personasDF.columns
        assert 'ITF'    in xhcols # x_h has ITF column (ingreso total familiar)
        assert 'CODUSU' in xhcols # x_h has CODUSU column (codigo de referencia de hogares)
        assert 'PONDIH' in xhcols # x_h has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
        assert 'IPCF'   in xhcols # x_h has IPCF column (ingreso per capita familiar del hogar)
        assert 'CODUSU' in xicols # x_i has CODUSU column (codigo de referencia de hogares)
        assert 'CH04'   in xicols # x_i has CH04 column (sexo)
        # assert 'CH05'   in xicols # x_i has CH05 column (fecha de nacimiento)
        assert 'CH06'   in xicols # x_i has CH06 column (edad, anios cumplidos)
        assert 'PONDIH' in xicols # x_i has PONDIH column (ponderador de ingresos, ajustado por no respuesta)
        # chequeamos que fecha sea un de las fechas para las que tenemos
        # el valor de la canasta basica alimentaria de referencia de varon adulto
        assert fecha in CBA.keys() 
    
        # filtramos el DataFrame y nos quedamos con uno nuevo que solo contenga
        # valores validos en la columna 'ITF'
        xh_clean = self.hogaresDF[self.hogaresDF['ITF'].notnull()]
    
        # armamos la columna 'refU' en el DataFrame de individuos
        refuCol = 'refU'
        if refuCol not in self.personasDF.columns:
            # si no existe lo creamos
            self.personasDF[refuCol] = self.personasDF.apply(indRefUmap, axis=1)
    
        # generamos un nuevo dataframe que agrega los refU por hogar
        x_ru = self.personasDF.groupby(['CODUSU', 'NRO_HOGAR']).agg({'refU': 'sum'})
    
        # agregamos la columna con el refu por hogar al DataFrame de hogares y
        # lo guardamos en un nuevo DataFrame
        if refuCol not in self.hogaresDF.columns:
            self.hogaresDF  = self.hogaresDF.merge(x_ru, how='inner', on=['CODUSU', 'NRO_HOGAR'])
    
        # calculamos los valores de canasta basica de referencia para cada hogar
        # queremos los valores para fecha:
        cbaCol = 'CBA_'+fecha.strftime('%Y%m%d')
        if cbaCol not in self.hogaresDF.columns:
            self.hogaresDF[cbaCol] = self.hogaresDF.apply(setCanasta, args=(fecha, 'a'), axis=1)
    
        # Hogares por debajo de la linea de indigencia:
        data_cba = self.hogaresDF[self.hogaresDF['ITF']<self.hogaresDF[cbaCol]]
    
        porc = 100*data_cba['PONDIH'].sum()/self.hogaresDF['PONDIH'].sum()
        
        if verbose:
            print()
            print('Porcentaje de Hogares por debajo de la Linea de Indigencia:')
            print('%4.2f'%(porc))
            print()
    
        return porc

    def pobreza_indigencia_total(self, fecha, save=True, verbose=False):
        """
        this method is a wraper to the call to each of the individual
        calculation methods of the class, to make all four calculations
        in one function. It adds the posibility of saving the result into
        the results dictionary.

        Parameters:
        ----------
        fecha: datetime.datetime,
        date for which indexes are being calculated.
        save: bool,
        if you want the result DataFrame to be saved as part of the
        result attribute of the class.
        verbose: bool,
        variable to indicate if th result is printed to std output.
 
        Returns:
        -------
        result: pd.DataFrame,
        in case save is False, the resulting DataFrame is given as output.
        If save is True, the method returns None.

        """

        res_dict = {'POBLACION_TOTAL': {}}
        res_dict['POBLACION_TOTAL']['indigencia_personas'] = self.indigencia_personas(fecha, verbose=verbose)
        res_dict['POBLACION_TOTAL']['indigencia_hogar'] = self.indigencia_hogar(fecha, verbose=verbose)
        res_dict['POBLACION_TOTAL']['pobreza_personas'] = self.pobreza_personas(fecha, verbose=verbose)
        res_dict['POBLACION_TOTAL']['pobreza_hogares'] = self.pobreza_hogar(fecha, verbose=verbose)

        if save:
            try:
                self.result[fecha]['POBLACION_TOTAL'] = res_dict
            except:
                self.result[fecha] = {}
                self.result[fecha]['POBLACION_TOTAL'] = res_dict

            return None

        else:
            return pd.DataFrame(res_dict).transpose()
        
    def pobreza_indigencia_col(self, col, fecha, save=True, verbose=False):
        """
        Description:
        this method takes the name of a column as input and
        calculates the poverty and indigence values of the population
        segregated by unique groups of values from the column name 
        values. For example if 'REGION' is passed, then each region's
        poverty and indigence indexes are calculated.

        Parameters:
        ----------
        col: str,
        valid column name of both self.hogaresDF and self.personasDF
        fecha: datetime.datetime,
        date for which indexes are being calculated.
        save: bool,
        if you want the result DataFrame to be saved as part of the
        result attribute of the class.
        verbose: bool,
        variable to indicate if th result is printed to std output.

        Returns:
        -------
        result: pd.DataFrame,
        in case save is False, the resulting DataFrame is given as output.
        If save is True, the method returns None.
        """
        
        res_dict = {}
        for data_h, data_i in zip(self.hogaresDF.groupby(col), self.personasDF.groupby(col)):
            # make deep copies of original DataFrames's slices
            dataH = deepcopy(data_h[1])
            dataI = deepcopy(data_i[1])

            # create empty results dict
            reg = data_h[0]
            res_dict[reg] = {}
            res_dict[reg]['indigencia_personas'] = indigencia_personas(dataH, dataI, fecha, verbose=verbose)
            res_dict[reg]['indigencia_hogares'] = indigencia_hogar(dataH, dataI, fecha, verbose=verbose)
            res_dict[reg]['pobreza_personas'] = pobreza_personas(dataH, dataI, fecha, verbose=verbose)
            res_dict[reg]['pobreza_hogares'] = pobreza_hogar(dataH, dataI, fecha, verbose=verbose)

        if save:
            try:
                self.result[fecha][col] = res_dict
            except:
                self.result[fecha] = {}
                self.result[fecha][col] = res_dict
                
            return None
        
        else:
            return pd.DataFrame(res_dict).transpose()

